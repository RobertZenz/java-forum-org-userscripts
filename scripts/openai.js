// ==UserScript==
// @name Java Forum OpenAI
// @description Eine einfache OpenAI Integration.
// @version 20230104185832
// @author Robert 'Bobby' Zenz
// @namespace https://gitlab.com/RobertZenz/java-forum-org-userscripts/
// @updateURL https://gitlab.com/RobertZenz/java-forum-org-userscripts/-/raw/main/scripts/openai.js
// @match *://*.java-forum.org/*
// @grant GM_addStyle
// @grant GM_deleteValue
// @grant GM_getValue
// @grant GM_listValues
// @grant GM_setValue
// ==/UserScript==

/*
 * Licensed under CC0.
 *
 * Meaning copyright has been waivered to the extent possible under law.
 */

// =============================================================================
// Luckily, Tampermonkey does not support using other userscripts
// as libraries, so copy and paste it is.
//
// https://github.com/Tampermonkey/tampermonkey/issues/853

let Colors = {
	blackWhite: function(colorHex) {
		if (Colors.brightness(colorHex) > 186) {
			return "#ffffff";
		} else {
			return "#000000";
		}
	},
	
	blend: function(baseColorHex, blendColorHex, blendStrength) {
		let baseColor = Colors.parseHex(baseColorHex);
		let blendColor = Colors.parseHex(blendColorHex);
		
		let blendedColor = {
			blue: Util.linear(baseColor.blue, blendColor.blue, blendStrength),
			green: Util.linear(baseColor.green, blendColor.green, blendStrength),
			red: Util.linear(baseColor.red, blendColor.red, blendStrength)
		};
		
		return Colors.toHex(blendedColor);
	},
	
	brightness: function(colorHex) {
		let color = Colors.parseHex(colorHex);
		let brightness = (color.red * 0.299) + (color.green * 0.587) + (color.blue * 0.114);
		
		return brightness;
	},
	
	invert: function (colorHex) {
		let color = Colors.parseHex(colorHex);
		color.red = 255 - color.red;
		color.green = 255 - color.green;
		color.blue = 255 - color.blue;
		
		return Colors.toHex(color);
	},
	
	parseHex: function(colorHex) {
		let preparedColorHex = colorHex;
		
		if (preparedColorHex.indexOf("#") === 0) {
			preparedColorHex = preparedColorHex.substr(1);
		}
		
		if (preparedColorHex.length === 3) {
			preparedColorHex = preparedColorHex[0] + preparedColorHex[0] + preparedColorHex[1] + preparedColorHex[1] + preparedColorHex[2] + preparedColorHex[2];
		}
		
		let red = parseInt(preparedColorHex.slice(0, 2), 16);
		let green = parseInt(preparedColorHex.slice(2, 4), 16);
		let blue = parseInt(preparedColorHex.slice(4, 6), 16);
		
		return  {
			blue: blue,
			green: green,
			red: red
		};
	},
	
	rgbToHex: function(rgbColor) {
		if (typeof rgbColor !== "string") {
			return "#ffffff";
		}
		
		let match = rgbColor.match(/rgb\(([0-9]+), ?([0-9]+), ?([0-9]+\))/);
		
		if (match === undefined || match === null) {
			return "#ffffff";
		}
		
		return Colors.toHex({
			blue: parseInt(match[3]),
			green: parseInt(match[2]),
			red: parseInt(match[1])
		});
	},
	
	toHex: function(color) {
		return "#" + Util.toHex(color.red) + Util.toHex(color.green) + Util.toHex(color.blue);
	}
};

let Events = {
	click: function(element, action) {
		element.addEventListener("click", function(mouseEvent) {
			mouseEvent.preventDefault();
			
			action(mouseEvent.currentTarget);
		});
	}
};

/**
 * The main library for interacting and changing the Java Forum.
 */
let JavaForum = {
	elementProcessors: [],
	memberTooltipGroups: [],
	memberTooltipListeners: [],
	menuTabPages: [],
	messageActions: [],
	messageListeners: [],
	popupParents: {},
	preferences: [],
	
	beginPrivateConversation: function(receiver, title, content) {
		unsafeWindow.location.href = "https://www.java-forum.org/unterhaltungen/add?to=" + encodeURIComponent(receiver) + "&title=" + encodeURIComponent(title) + "&message=" + encodeURIComponent(content);
	},
	
	createFromTemplate: function(templateName) {
		let templateElement = document.querySelector("template#" + templateName + "-template");
		
		if (templateElement === null) {
			JavaForum.error("Template <%s> was not found.",
					templateName);
		}
		
		let createdElement = templateElement.content.cloneNode(true);
		
		// Return the first non-text node.
		for (let templateChild of createdElement.childNodes) {
			if (templateChild.tagName !== undefined) {
				return templateChild;
			}
		}
		
		return createdElement;
	},
	
	closePopup: function(name) {
		for (let oldPopup of document.querySelectorAll("#" + name)) {
			oldPopup.parentNode.removeChild(oldPopup);
		}
		
		JavaForum.popupParents[name] = null;
	},
	
	createPopup: function(name) {
		let popupElement = JavaForum.createFromTemplate(name);
		
		JavaForum.initPopup(name, popupElement);
		
		return popupElement;
	},
	
	error: function(message, ...messageArguments) {
		let argumentCounter = 0;
		
		let formattedMessage = message.replace(/%[a-zA-Z]/g, function() {
			return messageArguments[argumentCounter++];
		});
		
		console.error(formattedMessage);
		
		throw formattedMessage;
	},
	
	extendMemberTooltip: function(memberTooltipElement) {
		let avatarElement = memberTooltipElement.querySelector("a.avatar");
		
		if (avatarElement === null) {
			return;
		}
		
		let userId = avatarElement.dataset.userId;
		let userName = memberTooltipElement.querySelector("a.username").innerText;
		
		memberTooltipElement.querySelector(".memberTooltip-header").dataset.index = -999999;
		memberTooltipElement.querySelector(".memberTooltip-info").dataset.index = 0;
		
		for (let memberTooltipGroup of JavaForum.memberTooltipGroups) {
			if (memberTooltipElement.querySelector(".member-tooltip-group-" + memberTooltipGroup.name) === null) {
				let memberTooltipGroupElement = JavaForum.createFromTemplate(memberTooltipGroup.groupTemplateName);
				memberTooltipGroupElement.classList.add("member-tooltip-group-" + memberTooltipGroup.name);
				memberTooltipGroupElement.dataset.id = userId;
				memberTooltipGroupElement.dataset.index = memberTooltipGroup.index;
				memberTooltipGroupElement.dataset.name = userName;
				
				if (memberTooltipGroup.index < 0) {
					Util.appendElementWithIndex(memberTooltipGroupElement, memberTooltipElement, memberTooltipGroup.index);
					Util.appendElementWithIndex(JavaForum.createFromTemplate("userscript-member-tooltip-group-separator"), memberTooltipElement, memberTooltipGroup.index);
				} else {
					Util.appendElementWithIndex(JavaForum.createFromTemplate("userscript-member-tooltip-group-separator"), memberTooltipElement, memberTooltipGroup.index);
					Util.appendElementWithIndex(memberTooltipGroupElement, memberTooltipElement, memberTooltipGroup.index);
				}
				
				if (typeof memberTooltipGroup.groupInitializer === "function") {
					memberTooltipGroup.groupInitializer(memberTooltipGroupElement, userId, userName);
				}
			}
		}
	},
	
	extendMessage: function(messageElement) {
		JavaForum.extendMessageActions(messageElement);
	},
	
	extendMessageActions: function(messageElement) {
		let messageActionBarElement = messageElement.querySelector(".message-actionBar");
		
		if (messageActionBarElement === undefined || messageActionBarElement === null) {
			return;
		}
		
		let actionBarSetExternalElement = messageActionBarElement.querySelector(".actionBar-set--external");
		let actionBarSetInternalElement = messageActionBarElement.querySelector(".actionBar-set--internal");
		
		for (let messageAction of JavaForum.messageActions) {
			let targetSetElement = actionBarSetExternalElement;
			
			if (messageAction.type === "internal") {
				targetSetElement = actionBarSetInternalElement;
			}
			
			if (targetSetElement.querySelector(".actionBar-action--" + messageAction.name) === null) {
				let actionElement = JavaForum.createFromTemplate("userscript-message-action");
				
				if (messageAction.icon !== undefined && messageAction.icon !== null && messageAction.icon !== "") {
					JavaForum.registerStylesheet(`
					.actionBar-action--` + messageAction.name + `::before {
						content: "\\` + messageAction.icon + `";
						width: 1.125em;
						display: inline-block;
						text-align: center;
						margin-right: 3px;
					}`);
				}
				
				actionElement.classList.add("actionBar-action--" + messageAction.name);
				actionElement.dataset.index = messageAction.index;
				actionElement.innerHTML = messageAction.text;
				actionElement.title = messageAction.tooltip;
				
				Events.click(actionElement, function() {
					messageAction.action(messageElement);
				});
				
				Util.appendElementWithIndex(actionElement, targetSetElement, messageAction.index);
			}
		}
	},
	
	focusPopup: function(popupElement) {
		let focusTargetElement = popupElement.querySelector(".userscript-popup-focus-this");
		
		if (focusTargetElement !== null) {
			focusTargetElement.focus();
			
			if (typeof focusTargetElement.select === "function") {
				focusTargetElement.select();
			}
		}
	},
	
	getCurrentUserId: function() {
		let currentUserAvatar = document.querySelector("div.p-navSticky--primary a.p-navgroup-link--user span.avatar");
		
		if (currentUserAvatar !== undefined && currentUserAvatar !== null) {
			let currentUserId = currentUserAvatar.dataset.userId;
			
			if (currentUserId !== undefined || currentUserId !== null) {
				return currentUserId;
			}
		}
		
		return -1;
	},
	
	getPreference: function(name) {
		let targetPreference = JavaForum.getRegisteredPreference(name);
		let value = GM_getValue("preference-" + name);
		
		if (value === undefined || value === null) {
			if (targetPreference !== null) {
				return targetPreference.defaultValue;
			}
		}
		
		if (targetPreference.type === "boolean") {
			value = !!value;
		} else if (targetPreference.type === "float") {
			value = Number.parseFloat(value);
		} else if (targetPreference.type === "int") {
			value = Number.parseInt(value);
		} else if (targetPreference.type === "string") {
			// Nothing to do.
		}
		
		return value;
	},
	
	getRegisteredPreference: function(name) {
		for (let preference of JavaForum.preferences) {
			if (preference.name === name) {
				return preference;
			}
		}
		
		return null;
	},
	
	initPopup: function(name, popupElement) {
		for (let closeButtonElement of popupElement.querySelectorAll(".userscript-popup-close-button")) {
			Events.click(closeButtonElement, function() {
				JavaForum.closePopup(name);
			});
		}
		
		for (let tabElement of popupElement.querySelectorAll(".userscript-popup-tab")) {
			Events.click(tabElement, function() {
				for (let tabElement of popupElement.querySelectorAll(".userscript-popup-tab")) {
					tabElement.classList.remove("is-active");
				}
				
				tabElement.classList.add("is-active");
				
				for (let tabPageElement of popupElement.querySelectorAll(".userscript-popup-tab-page")) {
					if (tabPageElement.id === tabElement.id) {
						tabPageElement.classList.remove("userscript-hidden");
						
						JavaForum.focusPopup(tabPageElement);
					} else {
						tabPageElement.classList.add("userscript-hidden");
					}
				}
			});
		}
		
		for (let tabPageElement of popupElement.querySelectorAll(".userscript-popup-tab-page")) {
			tabPageElement.classList.add("userscript-hidden");
		}
		
		return popupElement;
	},
	
	invokeElementProcessors: function(parentElement) {
		// Skip text nodes and similar.
		if (typeof parentElement.matches !== "function"
				|| typeof parentElement.querySelectorAll !== "function") {
			return;
		}
		
		for (let elementProcessor of JavaForum.elementProcessors) {
			if (parentElement.matches(elementProcessor.selector)) {
				elementProcessor.processorAction(parentElement);
			}
			
			for (let element of parentElement.querySelectorAll(elementProcessor.selector)) {
				elementProcessor.processorAction(element);
			}
		}
	},
	
	invokeMemberTooltipListeners: function(memberTooltipElement) {
		for (let listener of JavaForum.memberTooltipListeners) {
			listener(memberTooltipElement);
		}
	},
	
	invokeMessageListeners: function(messageElement) {
		for (let listener of JavaForum.messageListeners) {
			listener(messageElement);
		}
	},
	
	isPopupOpenAt: function(name, parentElement) {
		return JavaForum.popupParents[name] === parentElement;
	},
	
	registerElementProcessor: function(selector, processorAction) {
		JavaForum.elementProcessors.push({
			processorAction: processorAction,
			selector: selector,
		});
		
		for (let element of document.body.querySelectorAll(selector)) {
			processorAction(element);
		}
	},
	
	registerMemberTooltipGroup: function(name, index, groupTemplateName, groupInitializer) {
		JavaForum.memberTooltipGroups.push({
			groupInitializer: groupInitializer,
			groupTemplateName: groupTemplateName,
			index: index,
			name : name
		});
	},
	
	registerMemberTooltipListener: function(listener) {
		JavaForum.memberTooltipListeners.push(listener);
	},
	
	registerMenu: function(name, text, icon, index) {
		JavaForum.registerStylesheet(`
		.userscript-menu-button#` + name + ` {
			cursor: pointer;
		}
		
		.userscript-menu-button#` + name + ` > i::after {
			content: "\\` + icon + `";
		}
		`);
		
		let menuPopupName = name + "-menu-popup";
		
		let menuButton = JavaForum.createFromTemplate("userscript-menu-button");
		menuButton.id = name;
		menuButton.title = text;
		menuButton.dataset.index = index;
		Events.click(menuButton, function() {
			if (!JavaForum.isPopupOpenAt(menuPopupName, menuButton)) {
				JavaForum.closePopup(menuPopupName);
				
				let popupElement = JavaForum.createFromTemplate("userscript-menu-popup");
				popupElement.id = menuPopupName;
				
				for (let menuTabPage of JavaForum.menuTabPages) {
					let tabElement = JavaForum.createFromTemplate("userscript-popup-tab");
					tabElement.id = menuTabPage.name + "-tab";
					tabElement.title = menuTabPage.tooltip;
					tabElement.innerText = menuTabPage.text;
					
					let tabPageElement = JavaForum.createFromTemplate("userscript-popup-tab-page");
					tabPageElement.id = menuTabPage.name + "-tab";
					
					let tabPageContentElement = JavaForum.createFromTemplate(menuTabPage.pageTemplateName);
					
					if (typeof menuTabPage.pageInitializer === "function") {
						menuTabPage.pageInitializer(tabPageContentElement);
					}
					
					tabPageElement.appendChild(tabPageContentElement);
					
					popupElement.querySelector("#userscript-popup-tabs-container").appendChild(tabElement);
					popupElement.querySelector("#userscript-popup-tab-pages-container").appendChild(tabPageElement);
				}
				
				JavaForum.initPopup(menuPopupName, popupElement);
				
				JavaForum.showPopup(menuPopupName, popupElement, menuButton, "fixed");
			} else {
				JavaForum.closePopup(menuPopupName);
			}
		});
		
		Util.appendElementWithIndex(menuButton, document.body.querySelector("#top .p-navgroup.p-account"), index);
	},
	
	registerMenuTabPage: function(name, text, tooltip, index, pageTemplateName, pageInitializer) {
		JavaForum.menuTabPages.push({
			index: index,
			name : name,
			pageInitializer: pageInitializer,
			pageTemplateName: pageTemplateName,
			text: text,
			tooltip: tooltip
		});
		
		JavaForum.menuTabPages.sort(function(menuTabPageA, menuTabPageB) {
			return menuTabPageA.index - menuTabPageB.index;
		});
	},
	
	registerMessageAction: function(name, text, tooltip, icon, type, index, action) {
		JavaForum.messageActions.push({
			action: action,
			icon: icon,
			index: index,
			name : name,
			text: text,
			type: type,
			tooltip: tooltip
		});
		
		for (let messageElement of document.body.querySelectorAll(".message")) {
			JavaForum.extendMessageActions(messageElement);
		}
	},
	
	registerMessageListener: function(listener) {
		JavaForum.messageListeners.push(listener);
	},
	
	registerPreference: function(name, text, tooltip, type, defaultValue, changeListener) {
		JavaForum.preferences.push({
			changeListener: changeListener,
			defaultValue: defaultValue,
			name: name,
			text: text,
			tooltip: tooltip,
			type: type
		});
	},
	
	registerStylesheet: function(stylesheet) {
		GM_addStyle(stylesheet);
	},
	
	registerTemplate: function(name, content) {
		let template = document.createElement("template");
		template.id = name + "-template";
		template.innerHTML = content;
		
		document.body.appendChild(template);
	},
	
	setPreference: function(name, value) {
		GM_setValue("preference-" + name, value);
		
		let targetPreference = JavaForum.getRegisteredPreference(name);
		
		if (targetPreference !== null && typeof targetPreference.changeListener === "function") {
			targetPreference.changeListener(name, value);
		}
	},
	
	showPopup: function(name, popupElement, parentElement, type) {
		let parentBounds = parentElement.getBoundingClientRect();
		
		popupElement.style.position = type;
		
		document.body.appendChild(popupElement);
		
		let popupBounds = popupElement.getBoundingClientRect();
		
		if (type === "absolute") {
			popupElement.style.left = (window.scrollX + parentBounds.left - (popupBounds.width / 2) + (parentBounds.width / 2)) + "px";
			popupElement.style.top = (window.scrollY + parentBounds.bottom) + "px";
		} else {
			popupElement.style.left = (parentBounds.left - (popupBounds.width / 2) + (parentBounds.width / 2)) + "px";
			popupElement.style.top = parentBounds.bottom + "px";
		}
		
		popupBounds = popupElement.getBoundingClientRect();
		
		if (popupBounds.left < 0) {
			popupElement.style.left = 0 + "px";
		}
		
		if (popupBounds.top < 0) {
			popupElement.style.top = 0 + "px";
		}
		
		if (popupBounds.right > document.body.clientWidth) {
			popupElement.style.left = (document.body.clientWidth - popupBounds.width) + "px";
		}
		
		if (popupBounds.bottom > document.body.clientHeight) {
			popupElement.style.top = (document.body.clientHeight - popupBounds.height) + "px";
		}
		
		let popupArrow = popupElement.querySelector(".menu-arrow");
		popupArrow.style.left = (popupBounds.width / 2) + "px";
		
		JavaForum.popupParents[name] = parentElement;
		
		let tabToBeSelected = popupElement.querySelector(".userscript-popup-tab");
		
		if (tabToBeSelected !== null) {
			tabToBeSelected.click();
		} else {
			JavaForum.focusPopup(popupElement);
		}
		
		return popupElement;
	},
	
	togglePopup: function(name, parentElement, type) {
		if (!JavaForum.isPopupOpenAt(name, parentElement)) {
			JavaForum.closePopup(name);
			
			return JavaForum.showPopup(name, JavaForum.createPopup(name), parentElement, type);
		} else {
			JavaForum.closePopup(name);
			
			return null;
		}
	}
};

let Userscript = {
	clearData: function() {
		for (let key of GM_listValues()) {
			GM_deleteValue(key);
		}
	},
	
	getData: function() {
		let data = {};
		
		for (let key of GM_listValues()) {
			let value = GM_getValue(key);
			
			if (value !== undefined && value !== null && (!Array.isArray(value) || value.length > 0)) {
				data[key] = value;
			}
		}
		
		return data;
	}
};

let Util = {
	appendElementWithIndex: function(element, parentElement, index) {
		for (let existingElement of parentElement.childNodes) {
			if (existingElement.nodeType === Node.ELEMENT_NODE) {
				let currentIndex = 0;
				
				if (existingElement.dataset !== undefined && existingElement.dataset.index !== undefined) {
					currentIndex = Number.parseInt(existingElement.dataset.index);
				}
				
				if (currentIndex > index) {
					parentElement.insertBefore(element, existingElement);
					return;
				}
			}
		}
		
		if (index > 0) {
			parentElement.appendChild(element);
		} else if (parentElement.childNodes > 0) {
			parentElement.insertBefore(element, parentElement.childNodes[0]);
		} else {
			parentElement.appendChild(element);
		}
	},
	
	linear: function(valueA, valueB, strength) {
		return Math.round(valueA + ((valueB - valueA) * strength));
	},
	
	simplifyObject: function(object) {
		if (object === undefined || object === null) {
			return null;
		}
		
		let simplifiedObject = {};
		
		for (let property in object) {
			if (typeof object[property] === "function") {
				simplifiedObject[property] = object[property]();
			} else if (Array.isArray(object[property])) {
				let array = object[property];
				let simplifiedArray = [];
				
				for (let index = 0; index < array.length; index++) {
					simplifiedArray[index] = array[index];
				}
				
				simplifiedObject[property] = simplifiedArray;
			} else if (typeof object[property] === "object") {
				simplifiedObject[property] = Util.simplifyObject(object[property]);
			} else {
				simplifiedObject[property] = object[property];
			}
		}
		
		return simplifiedObject;
	},
	
	toJson: function(value) {
		return JSON.stringify(value, null, 4);
	},
	
	toHex: function(value) {
		if (value <= 0xf) {
			return "0" + value.toString(16);
		} else {
			return value.toString(16);
		}
	}
};

JavaForum.registerStylesheet(`
#userscript-menu-popup-data-editor {
	font-family: monospace;
	font-size: 0.8em;
	min-height: 15em;
	min-width: 15em;
}

.userscript-hidden {
	display: none;
	visibility: hidden;
}

.userscript-icon {
	font-family: "Font Awesome 5 Pro";
	font-size: 120%;
	font-style: normal;
	vertical-align: -.1em;
	margin: -0.255em 0px -0.255em 0;
}

.userscript-icon-with-text {
	margin: -0.255em 6px -0.255em 0;
}

.userscript-popup {
	background-color: #ffffff;
	border-radius: 3px;
	border-top: 3px solid #47a7eb;
	box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 10px 0px;
	z-index: 1000;
}

.userscript-popup-apply-button {
	float: right;
}

.userscript-popup-button-bar {
	background-color: #f6f6f6;
	border-top: 1px solid #e0e0e0;
	padding: 6px 15px;
}

.userscript-popup-close-button > .userscript-icon::after {
	content: "\\f0e2";
}

.userscript-popup-delete-button {
	background-color: #ff6228;
	border-color: #ff875b #ff6127 #ff6127 #ff875b;
}

.userscript-popup-delete-button:hover {
	background-color: #dd3e03 !important;
	border-color: #e86535 #dd3e03 #dd3e03 #e86535 !important;
}

.userscript-popup-delete-button > .userscript-icon::after {
	content: "\\f2ed";
}

.userscript-popup-hint-text {
	max-width: 30em;
}

.userscript-popup-main-area {
	padding: 6px 15px;
}

.userscript-popup-separator {
	margin: -1px 6px 0;
	border: 0px solid transparent;
	border-top: 1px solid #e0e0e0;
}

.userscript-preference-label {
	flex: 1;
	margin-bottom: auto;
	margin-right: 2em;
	margin-top: auto;
}

.userscript-preference-input {
	margin-left: auto;
}

.userscript-preference-row {
	border-radius: 3px;
	display: flex;
	padding: 0.2em;
}

.userscript-preference-row:hover {
	background-color: #f0f6ff;
}
`);

JavaForum.registerTemplate("userscript-member-tooltip-group-separator", `
<hr class="memberTooltip-separator">
`);

JavaForum.registerTemplate("userscript-menu-button", `
<a aria-expanded="false" aria-haspopup="true" aria-label="Tags" class="p-navgroup-link p-navgroup-link--iconic badgeContainer userscript-menu-button" href="">
	<i aria-hidden="true"></i>
	<span class="p-navgroup-linkText"></span>
</a>
`);

JavaForum.registerTemplate("userscript-menu-popup", `
<div class="userscript-popup">
	<span class="menu-arrow" style="left: 50%;"></span>
	<h4 class="menu-tabHeader tabs">
		<span class="hScroller">
			<span class="hScroller-scroll is-calculated" style="margin-bottom: -43px;" id="userscript-popup-tabs-container">
			</span>
		</span>
	</h4>
	<div id="userscript-popup-tab-pages-container">
	</div>
</div>
`);

JavaForum.registerTemplate("userscript-menu-popup-data-tab-page", `
<div>
	<form action="" autocomplete="off">
		<div class="userscript-popup-main-area">
			<textarea class="userscript-popup-focus-this" id="userscript-menu-popup-data-editor"></textarea>
		</div>
		<div class="userscript-popup-button-bar">
			<button class="button userscript-popup-close-button">
				<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
				<span>Schließen</span>
			</button>
			<button class="button userscript-popup-delete-button" id="userscript-menu-popup-data-delete-button">
				<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
				<span>Löschen</span>
			</button>
			<button class="button button--primary userscript-popup-apply-button" id="userscript-menu-popup-data-save-button">Speichern</button>
		</div>
	</form>
</div>
`);

JavaForum.registerTemplate("userscript-menu-popup-preferences-tab-page", `
<div>
	<form action="" autocomplete="off">
		<div class="userscript-popup-main-area">
		</div>
		<div class="userscript-popup-button-bar">
			<button class="button userscript-popup-close-button">
				<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
				<span>Schließen</span>
			</button>
		</div>
	</form>
</div>
`);

JavaForum.registerTemplate("userscript-message-action", `
<a href="" class="actionBar-action userscript-message-action-text-target" rel="nofollow"></a>
`);

JavaForum.registerTemplate("userscript-popup-tab", `
<a class="tabs-tab userscript-popup-tab" href="" tabindex="0"></a>
`);

JavaForum.registerTemplate("userscript-popup-tab-page", `
<div class="userscript-popup-tab-page"></div>
`);

JavaForum.registerMenuTabPage("preferences", "Einstellungen", "Die Einstellungen für dieses UserScript.", 9900, "userscript-menu-popup-preferences-tab-page", function(tabPageElement) {
	for (let preference of JavaForum.preferences) {
		let preferenceContainer = document.createElement("div");
		preferenceContainer.classList.add("userscript-preference-row");
		
		let labelElement = document.createElement("label");
		labelElement.classList.add("userscript-preference-label");
		labelElement.setAttribute("for", "preference-" + preference.name);
		labelElement.innerText = preference.text;
		labelElement.title = preference.tooltip;
		
		preferenceContainer.appendChild(labelElement);
		
		if (preference.type === "boolean") {
			let inputElement = document.createElement("input");
			inputElement.classList.add("userscript-preference-input");
			inputElement.checked = JavaForum.getPreference(preference.name);
			inputElement.id = "preference-" + preference.name;
			inputElement.type = "checkbox";
			inputElement.title = preference.tooltip;
			inputElement.addEventListener("change", function(changeEvent) {
				JavaForum.setPreference(preference.name, inputElement.checked);
			});
			
			preferenceContainer.appendChild(inputElement);
		} else if (preference.type === "float") {
			let inputElement = document.createElement("input");
			inputElement.classList.add("userscript-preference-input");
			inputElement.id = "preference-" + preference.name;
			inputElement.max = "1.00";
			inputElement.min = "0.00";
			inputElement.size = "6";
			inputElement.step = "0.01";
			inputElement.type = "number";
			inputElement.title = preference.tooltip;
			inputElement.value = JavaForum.getPreference(preference.name);
			inputElement.addEventListener("change", function(changeEvent) {
				JavaForum.setPreference(preference.name, inputElement.value);
			});
			
			preferenceContainer.appendChild(inputElement);
		} else if (preference.type === "int") {
			let inputElement = document.createElement("input");
			inputElement.classList.add("userscript-preference-input");
			inputElement.id = "preference-" + preference.name;
			inputElement.max = "999999";
			inputElement.min = "0";
			inputElement.size = "6";
			inputElement.step = "1";
			inputElement.type = "number";
			inputElement.title = preference.tooltip;
			inputElement.value = JavaForum.getPreference(preference.name);
			inputElement.addEventListener("change", function(changeEvent) {
				JavaForum.setPreference(preference.name, inputElement.value);
			});
			
			preferenceContainer.appendChild(inputElement);
		} else if (preference.type === "string") {
			let inputElement = document.createElement("input");
			inputElement.classList.add("userscript-preference-input");
			inputElement.id = "preference-" + preference.name;
			inputElement.type = "text";
			inputElement.title = preference.tooltip;
			inputElement.value = JavaForum.getPreference(preference.name);
			inputElement.addEventListener("change", function(changeEvent) {
				JavaForum.setPreference(preference.name, inputElement.value);
			});
			
			preferenceContainer.appendChild(inputElement);
		} else {
			JavaForum.error("Encountered unknown preference type <%s> on preference <%s>.",
					preference.type,
					preference.name);
		}
		
		tabPageElement.querySelector(".userscript-popup-main-area").appendChild(preferenceContainer);
	}
});

JavaForum.registerMenuTabPage("data", "Daten", "Die Roh-Daten welches das UserScript speichert.", 10000, "userscript-menu-popup-data-tab-page", function(tabPageElement) {
	tabPageElement.querySelector("#userscript-menu-popup-data-editor").value = Util.toJson(Userscript.getData());
	
	tabPageElement.querySelector("#userscript-menu-popup-data-delete-button").addEventListener("click", function(mouseEvent) {
		mouseEvent.preventDefault();
		
		if (confirm("Sollen wirklich alle Daten des Skripts gelöscht werden?")) {
			Userscript.clearData();
			
			if (confirm("Daten wurden alle gelöscht, soll die Seite jetzt neu geladen werden?")) {
				location.reload();
			}
		}
	});
	
	tabPageElement.querySelector("#userscript-menu-popup-data-save-button").addEventListener("click", function(mouseEvent) {
		mouseEvent.preventDefault();
		
		let data = JSON.parse(tabPageElement.querySelector("#userscript-menu-popup-data-editor").value);
		
		for (let key in data) {
			GM_setValue(key, data[key]);
		}
		
		if (confirm("Daten wurden importiert, soll die Seite jetzt neu geladen werden?")) {
			location.reload();
		}
	});
});

JavaForum.registerMemberTooltipListener(JavaForum.extendMemberTooltip);
JavaForum.registerMessageListener(JavaForum.extendMessage);

JavaForum.registerElementProcessor(".memberTooltip", JavaForum.invokeMemberTooltipListeners);
JavaForum.registerElementProcessor(".message", JavaForum.invokeMessageListeners);

new MutationObserver(function(mutationsList, observer) {
	for (let mutation of mutationsList) {
		if (mutation.type === 'childList') {
			for (let addedNode of mutation.addedNodes) {
				JavaForum.invokeElementProcessors(addedNode);
			}
		} else if (mutation.type === 'attributes') {
		}
	}
}).observe(document.body, {
	attributes: false,
	childList: true,
	subtree: true
});
// =============================================================================


let OpenAi = {
	performRequest: function(method, endpoint, body, successCallback, errorCallback) {
		let headers = {};
		
		headers["Authorization"] = "Bearer " + JavaForum.getPreference("token");
		headers["Content-Type"] = "application/json";
		
		fetch(JavaForum.getPreference("server-url") + endpoint, { method: method, headers: headers, mode: "cors", body: JSON.stringify(body) }).then(async function(response) {
			try {
				if (!response.ok) {
					if (errorCallback !== undefined && errorCallback !== null) {
						if (response.status === 401) {
							errorCallback(response, new Error("Für diese Operation ist eine gültige Authentifizierung erforderlich."));
						} else if (response.status === 500) {
							errorCallback(response, new Error("Der Server kann gerade die Anfrage nicht verarbeiten."));
						} else {
							errorCallback(response, new Error("Server antwortete mit Status <" + response.status + ">."));
						}
					}
				} else if (successCallback !== undefined && successCallback !== null) {
					let payloadRaw = await response.text();
					
					if (payloadRaw !== null && payloadRaw !== "") {
						let payload = JSON.parse(payloadRaw);
						
						successCallback(response, payload);
					} else {
						successCallback(response, null);
					}
				}
			} catch (error) {
				console.error(error);
				
				if (errorCallback !== undefined && errorCallback !== null) {
					try {
						errorCallback(response, error);
					} catch (callbackError) {
						console.error(callbackError);
					}
				}
			}
		}).catch((error) => {
			console.error(error);
			
			if (errorCallback !== undefined && errorCallback !== null) {
				try {
					errorCallback(null, error);
				} catch (callbackError) {
					console.error(callbackError);
				}
			}
		});
	},
	
	sendQuery: function(text, successCallback, errorCallback) {
		let body = {
			"max_tokens": JavaForum.getPreference("max-tokens"),
			"model": JavaForum.getPreference("model"),
			"prompt": text
		};
		
		OpenAi.performRequest("POST", "/v1/completions", body, successCallback, errorCallback);
	}
};

JavaForum.registerStylesheet(`
.openai-conversation {
	background-color: #f5f8fc;
	padding: 10px;
	border-top: 1px solid #cccccc;
}

.openai-conversation-button-bar {
}

.openai-conversation-close-button {
	display: inline-block;
	margin-top: 10px;
}

.openai-conversation-openai-message {
	background-color: #d8e6f3;
	font-size: 90%;
	margin: 1px;
	max-height: 15em;
	overflow: auto;
	padding: 4px;
	white-space: break-spaces;
}

.openai-conversation-user-container {
	margin-top: 10px;
}

.openai-conversation-user-input {
	display: block;
	min-height: 6em;
	width: 100%;
}

.openai-conversation-user-message {
	background-color: #d8d8d8;
	font-size: 90%;
	margin: 1px;
	max-height: 15em;
	overflow: auto;
	padding: 4px;
	white-space: break-spaces;
}

.openai-conversation-user-send-button {
	display: block;
	float: right;
	margin-top: 10px;
}
`);

JavaForum.registerTemplate("action", `
<a href="" class="actionBar-action actionBar-action--openai" title="Als Anfrage an OpenAI senden" rel="nofollow">OpenAI</a>
`);

JavaForum.registerTemplate("openai-conversation", `
<div class="openai-conversation">
	<div class="openai-conversation-messages-container"></div>
	<div class="openai-conversation-user-container">
		<textarea class="fr-basic fr-box openai-conversation-user-input" placeholder="Hier eine weitere Frage eingeben..."></textarea>
		<div class="openai-conversation-button-bar">
			<button class="button button--link openai-conversation-close-button">Schließen</button>
			<button class="button button--primary openai-conversation-user-send-button">Senden</button>
		</div>
	</div>
</div>
`);

JavaForum.registerTemplate("openai-conversation-openai-message", `
<pre class="openai-conversation-openai-message">
... ... ... ... ... ... ... Bitte warten ... ... ... ... ... ... ...
</pre>
`);

JavaForum.registerTemplate("openai-conversation-user-message", `
<pre class="openai-conversation-user-message"></pre>
`);

JavaForum.registerPreference("server-url", "Server", "Die Adresse des OpenAI Servers.", "string", "https://api.openai.com", null);
JavaForum.registerPreference("token", "Token", "Der Token für den Zugriff.", "string", "", null);
JavaForum.registerPreference("model", "Modell", "Das (Text-)Modell welches verwendet werden soll.", "string", "text-davinci-003", null);
JavaForum.registerPreference("max-tokens", "Maximale Länge", "Die maximale Länge einer Antwort von OpenAI.", "int", 1024, null);

JavaForum.registerMenu("openai", "OpenAI", "f2db", 9000);

JavaForum.registerMessageAction("openai", "OpenAI", "Diesen Beitrag direkt an OpenAI senden und die Antwort anzeigen.", "f2db", "external", -1000, function(messageElement) {
	let conversationElement = messageElement.querySelector(".openai-conversation");
	
	if (conversationElement === undefined || conversationElement === null) {
		conversationElement = JavaForum.createFromTemplate("openai-conversation");
		
		let messageContainerElement = conversationElement.querySelector(".openai-conversation-messages-container");
		
		Events.click(conversationElement.querySelector(".openai-conversation-close-button"), function() {
			conversationElement.parentNode.removeChild(conversationElement);
		});
		
		Events.click(conversationElement.querySelector(".openai-conversation-user-send-button"), function() {
			let userText = conversationElement.querySelector(".openai-conversation-user-input").value.trim();
			
			if (userText === "") {
				return;
			}
			
			conversationElement.querySelector(".openai-conversation-user-input").value = "";
			
			let userElement = JavaForum.createFromTemplate("openai-conversation-user-message");
			userElement.textContent = userText;
			messageContainerElement.appendChild(userElement);
			
			let responseElement = JavaForum.createFromTemplate("openai-conversation-openai-message");
			messageContainerElement.appendChild(responseElement);
			
			OpenAi.sendQuery(userText, function(response, payload) {
				responseElement.textContent = payload.choices[0].text.trim();
			}, function (response, error) {
				responseElement.textContent = "Fehler: " + error;
			});
		});
		
		messageElement.appendChild(conversationElement);
		
		conversationElement.querySelector(".openai-conversation-user-input").value = messageElement.querySelector(".message-body").textContent.trim();
	}
});
