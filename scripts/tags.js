// ==UserScript==
// @name Java Forum Tags
// @description Ein Tag-System fuer das Java Forum.
// @version 20230104185832
// @author Robert 'Bobby' Zenz
// @namespace https://gitlab.com/RobertZenz/java-forum-org-userscripts/
// @updateURL https://gitlab.com/RobertZenz/java-forum-org-userscripts/-/raw/main/scripts/tags.js
// @match *://*.java-forum.org/*
// @grant GM_addStyle
// @grant GM_deleteValue
// @grant GM_getValue
// @grant GM_listValues
// @grant GM_setValue
// ==/UserScript==

/*
 * Licensed under CC0.
 *
 * Meaning copyright has been waivered to the extent possible under law.
 */

// =============================================================================
// Luckily, Tampermonkey does not support using other userscripts
// as libraries, so copy and paste it is.
//
// https://github.com/Tampermonkey/tampermonkey/issues/853

let Colors = {
	blackWhite: function(colorHex) {
		if (Colors.brightness(colorHex) > 186) {
			return "#ffffff";
		} else {
			return "#000000";
		}
	},
	
	blend: function(baseColorHex, blendColorHex, blendStrength) {
		let baseColor = Colors.parseHex(baseColorHex);
		let blendColor = Colors.parseHex(blendColorHex);
		
		let blendedColor = {
			blue: Util.linear(baseColor.blue, blendColor.blue, blendStrength),
			green: Util.linear(baseColor.green, blendColor.green, blendStrength),
			red: Util.linear(baseColor.red, blendColor.red, blendStrength)
		};
		
		return Colors.toHex(blendedColor);
	},
	
	brightness: function(colorHex) {
		let color = Colors.parseHex(colorHex);
		let brightness = (color.red * 0.299) + (color.green * 0.587) + (color.blue * 0.114);
		
		return brightness;
	},
	
	invert: function (colorHex) {
		let color = Colors.parseHex(colorHex);
		color.red = 255 - color.red;
		color.green = 255 - color.green;
		color.blue = 255 - color.blue;
		
		return Colors.toHex(color);
	},
	
	parseHex: function(colorHex) {
		let preparedColorHex = colorHex;
		
		if (preparedColorHex.indexOf("#") === 0) {
			preparedColorHex = preparedColorHex.substr(1);
		}
		
		if (preparedColorHex.length === 3) {
			preparedColorHex = preparedColorHex[0] + preparedColorHex[0] + preparedColorHex[1] + preparedColorHex[1] + preparedColorHex[2] + preparedColorHex[2];
		}
		
		let red = parseInt(preparedColorHex.slice(0, 2), 16);
		let green = parseInt(preparedColorHex.slice(2, 4), 16);
		let blue = parseInt(preparedColorHex.slice(4, 6), 16);
		
		return  {
			blue: blue,
			green: green,
			red: red
		};
	},
	
	rgbToHex: function(rgbColor) {
		if (typeof rgbColor !== "string") {
			return "#ffffff";
		}
		
		let match = rgbColor.match(/rgb\(([0-9]+), ?([0-9]+), ?([0-9]+\))/);
		
		if (match === undefined || match === null) {
			return "#ffffff";
		}
		
		return Colors.toHex({
			blue: parseInt(match[3]),
			green: parseInt(match[2]),
			red: parseInt(match[1])
		});
	},
	
	toHex: function(color) {
		return "#" + Util.toHex(color.red) + Util.toHex(color.green) + Util.toHex(color.blue);
	}
};

let Events = {
	click: function(element, action) {
		element.addEventListener("click", function(mouseEvent) {
			mouseEvent.preventDefault();
			
			action(mouseEvent.currentTarget);
		});
	}
};

/**
 * The main library for interacting and changing the Java Forum.
 */
let JavaForum = {
	elementProcessors: [],
	memberTooltipGroups: [],
	memberTooltipListeners: [],
	menuTabPages: [],
	messageActions: [],
	messageListeners: [],
	popupParents: {},
	preferences: [],
	
	beginPrivateConversation: function(receiver, title, content) {
		unsafeWindow.location.href = "https://www.java-forum.org/unterhaltungen/add?to=" + encodeURIComponent(receiver) + "&title=" + encodeURIComponent(title) + "&message=" + encodeURIComponent(content);
	},
	
	createFromTemplate: function(templateName) {
		let templateElement = document.querySelector("template#" + templateName + "-template");
		
		if (templateElement === null) {
			JavaForum.error("Template <%s> was not found.",
					templateName);
		}
		
		let createdElement = templateElement.content.cloneNode(true);
		
		// Return the first non-text node.
		for (let templateChild of createdElement.childNodes) {
			if (templateChild.tagName !== undefined) {
				return templateChild;
			}
		}
		
		return createdElement;
	},
	
	closePopup: function(name) {
		for (let oldPopup of document.querySelectorAll("#" + name)) {
			oldPopup.parentNode.removeChild(oldPopup);
		}
		
		JavaForum.popupParents[name] = null;
	},
	
	createPopup: function(name) {
		let popupElement = JavaForum.createFromTemplate(name);
		
		JavaForum.initPopup(name, popupElement);
		
		return popupElement;
	},
	
	error: function(message, ...messageArguments) {
		let argumentCounter = 0;
		
		let formattedMessage = message.replace(/%[a-zA-Z]/g, function() {
			return messageArguments[argumentCounter++];
		});
		
		console.error(formattedMessage);
		
		throw formattedMessage;
	},
	
	extendMemberTooltip: function(memberTooltipElement) {
		let avatarElement = memberTooltipElement.querySelector("a.avatar");
		
		if (avatarElement === null) {
			return;
		}
		
		let userId = avatarElement.dataset.userId;
		let userName = memberTooltipElement.querySelector("a.username").innerText;
		
		memberTooltipElement.querySelector(".memberTooltip-header").dataset.index = -999999;
		memberTooltipElement.querySelector(".memberTooltip-info").dataset.index = 0;
		
		for (let memberTooltipGroup of JavaForum.memberTooltipGroups) {
			if (memberTooltipElement.querySelector(".member-tooltip-group-" + memberTooltipGroup.name) === null) {
				let memberTooltipGroupElement = JavaForum.createFromTemplate(memberTooltipGroup.groupTemplateName);
				memberTooltipGroupElement.classList.add("member-tooltip-group-" + memberTooltipGroup.name);
				memberTooltipGroupElement.dataset.id = userId;
				memberTooltipGroupElement.dataset.index = memberTooltipGroup.index;
				memberTooltipGroupElement.dataset.name = userName;
				
				if (memberTooltipGroup.index < 0) {
					Util.appendElementWithIndex(memberTooltipGroupElement, memberTooltipElement, memberTooltipGroup.index);
					Util.appendElementWithIndex(JavaForum.createFromTemplate("userscript-member-tooltip-group-separator"), memberTooltipElement, memberTooltipGroup.index);
				} else {
					Util.appendElementWithIndex(JavaForum.createFromTemplate("userscript-member-tooltip-group-separator"), memberTooltipElement, memberTooltipGroup.index);
					Util.appendElementWithIndex(memberTooltipGroupElement, memberTooltipElement, memberTooltipGroup.index);
				}
				
				if (typeof memberTooltipGroup.groupInitializer === "function") {
					memberTooltipGroup.groupInitializer(memberTooltipGroupElement, userId, userName);
				}
			}
		}
	},
	
	extendMessage: function(messageElement) {
		JavaForum.extendMessageActions(messageElement);
	},
	
	extendMessageActions: function(messageElement) {
		let messageActionBarElement = messageElement.querySelector(".message-actionBar");
		
		if (messageActionBarElement === undefined || messageActionBarElement === null) {
			return;
		}
		
		let actionBarSetExternalElement = messageActionBarElement.querySelector(".actionBar-set--external");
		let actionBarSetInternalElement = messageActionBarElement.querySelector(".actionBar-set--internal");
		
		for (let messageAction of JavaForum.messageActions) {
			let targetSetElement = actionBarSetExternalElement;
			
			if (messageAction.type === "internal") {
				targetSetElement = actionBarSetInternalElement;
			}
			
			if (targetSetElement.querySelector(".actionBar-action--" + messageAction.name) === null) {
				let actionElement = JavaForum.createFromTemplate("userscript-message-action");
				
				if (messageAction.icon !== undefined && messageAction.icon !== null && messageAction.icon !== "") {
					JavaForum.registerStylesheet(`
					.actionBar-action--` + messageAction.name + `::before {
						content: "\\` + messageAction.icon + `";
						width: 1.125em;
						display: inline-block;
						text-align: center;
						margin-right: 3px;
					}`);
				}
				
				actionElement.classList.add("actionBar-action--" + messageAction.name);
				actionElement.dataset.index = messageAction.index;
				actionElement.innerHTML = messageAction.text;
				actionElement.title = messageAction.tooltip;
				
				Events.click(actionElement, function() {
					messageAction.action(messageElement);
				});
				
				Util.appendElementWithIndex(actionElement, targetSetElement, messageAction.index);
			}
		}
	},
	
	focusPopup: function(popupElement) {
		let focusTargetElement = popupElement.querySelector(".userscript-popup-focus-this");
		
		if (focusTargetElement !== null) {
			focusTargetElement.focus();
			
			if (typeof focusTargetElement.select === "function") {
				focusTargetElement.select();
			}
		}
	},
	
	getCurrentUserId: function() {
		let currentUserAvatar = document.querySelector("div.p-navSticky--primary a.p-navgroup-link--user span.avatar");
		
		if (currentUserAvatar !== undefined && currentUserAvatar !== null) {
			let currentUserId = currentUserAvatar.dataset.userId;
			
			if (currentUserId !== undefined || currentUserId !== null) {
				return currentUserId;
			}
		}
		
		return -1;
	},
	
	getPreference: function(name) {
		let targetPreference = JavaForum.getRegisteredPreference(name);
		let value = GM_getValue("preference-" + name);
		
		if (value === undefined || value === null) {
			if (targetPreference !== null) {
				return targetPreference.defaultValue;
			}
		}
		
		if (targetPreference.type === "boolean") {
			value = !!value;
		} else if (targetPreference.type === "float") {
			value = Number.parseFloat(value);
		} else if (targetPreference.type === "int") {
			value = Number.parseInt(value);
		} else if (targetPreference.type === "string") {
			// Nothing to do.
		}
		
		return value;
	},
	
	getRegisteredPreference: function(name) {
		for (let preference of JavaForum.preferences) {
			if (preference.name === name) {
				return preference;
			}
		}
		
		return null;
	},
	
	initPopup: function(name, popupElement) {
		for (let closeButtonElement of popupElement.querySelectorAll(".userscript-popup-close-button")) {
			Events.click(closeButtonElement, function() {
				JavaForum.closePopup(name);
			});
		}
		
		for (let tabElement of popupElement.querySelectorAll(".userscript-popup-tab")) {
			Events.click(tabElement, function() {
				for (let tabElement of popupElement.querySelectorAll(".userscript-popup-tab")) {
					tabElement.classList.remove("is-active");
				}
				
				tabElement.classList.add("is-active");
				
				for (let tabPageElement of popupElement.querySelectorAll(".userscript-popup-tab-page")) {
					if (tabPageElement.id === tabElement.id) {
						tabPageElement.classList.remove("userscript-hidden");
						
						JavaForum.focusPopup(tabPageElement);
					} else {
						tabPageElement.classList.add("userscript-hidden");
					}
				}
			});
		}
		
		for (let tabPageElement of popupElement.querySelectorAll(".userscript-popup-tab-page")) {
			tabPageElement.classList.add("userscript-hidden");
		}
		
		return popupElement;
	},
	
	invokeElementProcessors: function(parentElement) {
		// Skip text nodes and similar.
		if (typeof parentElement.matches !== "function"
				|| typeof parentElement.querySelectorAll !== "function") {
			return;
		}
		
		for (let elementProcessor of JavaForum.elementProcessors) {
			if (parentElement.matches(elementProcessor.selector)) {
				elementProcessor.processorAction(parentElement);
			}
			
			for (let element of parentElement.querySelectorAll(elementProcessor.selector)) {
				elementProcessor.processorAction(element);
			}
		}
	},
	
	invokeMemberTooltipListeners: function(memberTooltipElement) {
		for (let listener of JavaForum.memberTooltipListeners) {
			listener(memberTooltipElement);
		}
	},
	
	invokeMessageListeners: function(messageElement) {
		for (let listener of JavaForum.messageListeners) {
			listener(messageElement);
		}
	},
	
	isPopupOpenAt: function(name, parentElement) {
		return JavaForum.popupParents[name] === parentElement;
	},
	
	registerElementProcessor: function(selector, processorAction) {
		JavaForum.elementProcessors.push({
			processorAction: processorAction,
			selector: selector,
		});
		
		for (let element of document.body.querySelectorAll(selector)) {
			processorAction(element);
		}
	},
	
	registerMemberTooltipGroup: function(name, index, groupTemplateName, groupInitializer) {
		JavaForum.memberTooltipGroups.push({
			groupInitializer: groupInitializer,
			groupTemplateName: groupTemplateName,
			index: index,
			name : name
		});
	},
	
	registerMemberTooltipListener: function(listener) {
		JavaForum.memberTooltipListeners.push(listener);
	},
	
	registerMenu: function(name, text, icon, index) {
		JavaForum.registerStylesheet(`
		.userscript-menu-button#` + name + ` {
			cursor: pointer;
		}
		
		.userscript-menu-button#` + name + ` > i::after {
			content: "\\` + icon + `";
		}
		`);
		
		let menuPopupName = name + "-menu-popup";
		
		let menuButton = JavaForum.createFromTemplate("userscript-menu-button");
		menuButton.id = name;
		menuButton.title = text;
		menuButton.dataset.index = index;
		Events.click(menuButton, function() {
			if (!JavaForum.isPopupOpenAt(menuPopupName, menuButton)) {
				JavaForum.closePopup(menuPopupName);
				
				let popupElement = JavaForum.createFromTemplate("userscript-menu-popup");
				popupElement.id = menuPopupName;
				
				for (let menuTabPage of JavaForum.menuTabPages) {
					let tabElement = JavaForum.createFromTemplate("userscript-popup-tab");
					tabElement.id = menuTabPage.name + "-tab";
					tabElement.title = menuTabPage.tooltip;
					tabElement.innerText = menuTabPage.text;
					
					let tabPageElement = JavaForum.createFromTemplate("userscript-popup-tab-page");
					tabPageElement.id = menuTabPage.name + "-tab";
					
					let tabPageContentElement = JavaForum.createFromTemplate(menuTabPage.pageTemplateName);
					
					if (typeof menuTabPage.pageInitializer === "function") {
						menuTabPage.pageInitializer(tabPageContentElement);
					}
					
					tabPageElement.appendChild(tabPageContentElement);
					
					popupElement.querySelector("#userscript-popup-tabs-container").appendChild(tabElement);
					popupElement.querySelector("#userscript-popup-tab-pages-container").appendChild(tabPageElement);
				}
				
				JavaForum.initPopup(menuPopupName, popupElement);
				
				JavaForum.showPopup(menuPopupName, popupElement, menuButton, "fixed");
			} else {
				JavaForum.closePopup(menuPopupName);
			}
		});
		
		Util.appendElementWithIndex(menuButton, document.body.querySelector("#top .p-navgroup.p-account"), index);
	},
	
	registerMenuTabPage: function(name, text, tooltip, index, pageTemplateName, pageInitializer) {
		JavaForum.menuTabPages.push({
			index: index,
			name : name,
			pageInitializer: pageInitializer,
			pageTemplateName: pageTemplateName,
			text: text,
			tooltip: tooltip
		});
		
		JavaForum.menuTabPages.sort(function(menuTabPageA, menuTabPageB) {
			return menuTabPageA.index - menuTabPageB.index;
		});
	},
	
	registerMessageAction: function(name, text, tooltip, icon, type, index, action) {
		JavaForum.messageActions.push({
			action: action,
			icon: icon,
			index: index,
			name : name,
			text: text,
			type: type,
			tooltip: tooltip
		});
		
		for (let messageElement of document.body.querySelectorAll(".message")) {
			JavaForum.extendMessageActions(messageElement);
		}
	},
	
	registerMessageListener: function(listener) {
		JavaForum.messageListeners.push(listener);
	},
	
	registerPreference: function(name, text, tooltip, type, defaultValue, changeListener) {
		JavaForum.preferences.push({
			changeListener: changeListener,
			defaultValue: defaultValue,
			name: name,
			text: text,
			tooltip: tooltip,
			type: type
		});
	},
	
	registerStylesheet: function(stylesheet) {
		GM_addStyle(stylesheet);
	},
	
	registerTemplate: function(name, content) {
		let template = document.createElement("template");
		template.id = name + "-template";
		template.innerHTML = content;
		
		document.body.appendChild(template);
	},
	
	setPreference: function(name, value) {
		GM_setValue("preference-" + name, value);
		
		let targetPreference = JavaForum.getRegisteredPreference(name);
		
		if (targetPreference !== null && typeof targetPreference.changeListener === "function") {
			targetPreference.changeListener(name, value);
		}
	},
	
	showPopup: function(name, popupElement, parentElement, type) {
		let parentBounds = parentElement.getBoundingClientRect();
		
		popupElement.style.position = type;
		
		document.body.appendChild(popupElement);
		
		let popupBounds = popupElement.getBoundingClientRect();
		
		if (type === "absolute") {
			popupElement.style.left = (window.scrollX + parentBounds.left - (popupBounds.width / 2) + (parentBounds.width / 2)) + "px";
			popupElement.style.top = (window.scrollY + parentBounds.bottom) + "px";
		} else {
			popupElement.style.left = (parentBounds.left - (popupBounds.width / 2) + (parentBounds.width / 2)) + "px";
			popupElement.style.top = parentBounds.bottom + "px";
		}
		
		popupBounds = popupElement.getBoundingClientRect();
		
		if (popupBounds.left < 0) {
			popupElement.style.left = 0 + "px";
		}
		
		if (popupBounds.top < 0) {
			popupElement.style.top = 0 + "px";
		}
		
		if (popupBounds.right > document.body.clientWidth) {
			popupElement.style.left = (document.body.clientWidth - popupBounds.width) + "px";
		}
		
		if (popupBounds.bottom > document.body.clientHeight) {
			popupElement.style.top = (document.body.clientHeight - popupBounds.height) + "px";
		}
		
		let popupArrow = popupElement.querySelector(".menu-arrow");
		popupArrow.style.left = (popupBounds.width / 2) + "px";
		
		JavaForum.popupParents[name] = parentElement;
		
		let tabToBeSelected = popupElement.querySelector(".userscript-popup-tab");
		
		if (tabToBeSelected !== null) {
			tabToBeSelected.click();
		} else {
			JavaForum.focusPopup(popupElement);
		}
		
		return popupElement;
	},
	
	togglePopup: function(name, parentElement, type) {
		if (!JavaForum.isPopupOpenAt(name, parentElement)) {
			JavaForum.closePopup(name);
			
			return JavaForum.showPopup(name, JavaForum.createPopup(name), parentElement, type);
		} else {
			JavaForum.closePopup(name);
			
			return null;
		}
	}
};

let Userscript = {
	clearData: function() {
		for (let key of GM_listValues()) {
			GM_deleteValue(key);
		}
	},
	
	getData: function() {
		let data = {};
		
		for (let key of GM_listValues()) {
			let value = GM_getValue(key);
			
			if (value !== undefined && value !== null && (!Array.isArray(value) || value.length > 0)) {
				data[key] = value;
			}
		}
		
		return data;
	}
};

let Util = {
	appendElementWithIndex: function(element, parentElement, index) {
		for (let existingElement of parentElement.childNodes) {
			if (existingElement.nodeType === Node.ELEMENT_NODE) {
				let currentIndex = 0;
				
				if (existingElement.dataset !== undefined && existingElement.dataset.index !== undefined) {
					currentIndex = Number.parseInt(existingElement.dataset.index);
				}
				
				if (currentIndex > index) {
					parentElement.insertBefore(element, existingElement);
					return;
				}
			}
		}
		
		if (index > 0) {
			parentElement.appendChild(element);
		} else if (parentElement.childNodes > 0) {
			parentElement.insertBefore(element, parentElement.childNodes[0]);
		} else {
			parentElement.appendChild(element);
		}
	},
	
	linear: function(valueA, valueB, strength) {
		return Math.round(valueA + ((valueB - valueA) * strength));
	},
	
	simplifyObject: function(object) {
		if (object === undefined || object === null) {
			return null;
		}
		
		let simplifiedObject = {};
		
		for (let property in object) {
			if (typeof object[property] === "function") {
				simplifiedObject[property] = object[property]();
			} else if (Array.isArray(object[property])) {
				let array = object[property];
				let simplifiedArray = [];
				
				for (let index = 0; index < array.length; index++) {
					simplifiedArray[index] = array[index];
				}
				
				simplifiedObject[property] = simplifiedArray;
			} else if (typeof object[property] === "object") {
				simplifiedObject[property] = Util.simplifyObject(object[property]);
			} else {
				simplifiedObject[property] = object[property];
			}
		}
		
		return simplifiedObject;
	},
	
	toJson: function(value) {
		return JSON.stringify(value, null, 4);
	},
	
	toHex: function(value) {
		if (value <= 0xf) {
			return "0" + value.toString(16);
		} else {
			return value.toString(16);
		}
	}
};

JavaForum.registerStylesheet(`
#userscript-menu-popup-data-editor {
	font-family: monospace;
	font-size: 0.8em;
	min-height: 15em;
	min-width: 15em;
}

.userscript-hidden {
	display: none;
	visibility: hidden;
}

.userscript-icon {
	font-family: "Font Awesome 5 Pro";
	font-size: 120%;
	font-style: normal;
	vertical-align: -.1em;
	margin: -0.255em 0px -0.255em 0;
}

.userscript-icon-with-text {
	margin: -0.255em 6px -0.255em 0;
}

.userscript-popup {
	background-color: #ffffff;
	border-radius: 3px;
	border-top: 3px solid #47a7eb;
	box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 10px 0px;
	z-index: 1000;
}

.userscript-popup-apply-button {
	float: right;
}

.userscript-popup-button-bar {
	background-color: #f6f6f6;
	border-top: 1px solid #e0e0e0;
	padding: 6px 15px;
}

.userscript-popup-close-button > .userscript-icon::after {
	content: "\\f0e2";
}

.userscript-popup-delete-button {
	background-color: #ff6228;
	border-color: #ff875b #ff6127 #ff6127 #ff875b;
}

.userscript-popup-delete-button:hover {
	background-color: #dd3e03 !important;
	border-color: #e86535 #dd3e03 #dd3e03 #e86535 !important;
}

.userscript-popup-delete-button > .userscript-icon::after {
	content: "\\f2ed";
}

.userscript-popup-hint-text {
	max-width: 30em;
}

.userscript-popup-main-area {
	padding: 6px 15px;
}

.userscript-popup-separator {
	margin: -1px 6px 0;
	border: 0px solid transparent;
	border-top: 1px solid #e0e0e0;
}

.userscript-preference-label {
	flex: 1;
	margin-bottom: auto;
	margin-right: 2em;
	margin-top: auto;
}

.userscript-preference-input {
	margin-left: auto;
}

.userscript-preference-row {
	border-radius: 3px;
	display: flex;
	padding: 0.2em;
}

.userscript-preference-row:hover {
	background-color: #f0f6ff;
}
`);

JavaForum.registerTemplate("userscript-member-tooltip-group-separator", `
<hr class="memberTooltip-separator">
`);

JavaForum.registerTemplate("userscript-menu-button", `
<a aria-expanded="false" aria-haspopup="true" aria-label="Tags" class="p-navgroup-link p-navgroup-link--iconic badgeContainer userscript-menu-button" href="">
	<i aria-hidden="true"></i>
	<span class="p-navgroup-linkText"></span>
</a>
`);

JavaForum.registerTemplate("userscript-menu-popup", `
<div class="userscript-popup">
	<span class="menu-arrow" style="left: 50%;"></span>
	<h4 class="menu-tabHeader tabs">
		<span class="hScroller">
			<span class="hScroller-scroll is-calculated" style="margin-bottom: -43px;" id="userscript-popup-tabs-container">
			</span>
		</span>
	</h4>
	<div id="userscript-popup-tab-pages-container">
	</div>
</div>
`);

JavaForum.registerTemplate("userscript-menu-popup-data-tab-page", `
<div>
	<form action="" autocomplete="off">
		<div class="userscript-popup-main-area">
			<textarea class="userscript-popup-focus-this" id="userscript-menu-popup-data-editor"></textarea>
		</div>
		<div class="userscript-popup-button-bar">
			<button class="button userscript-popup-close-button">
				<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
				<span>Schließen</span>
			</button>
			<button class="button userscript-popup-delete-button" id="userscript-menu-popup-data-delete-button">
				<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
				<span>Löschen</span>
			</button>
			<button class="button button--primary userscript-popup-apply-button" id="userscript-menu-popup-data-save-button">Speichern</button>
		</div>
	</form>
</div>
`);

JavaForum.registerTemplate("userscript-menu-popup-preferences-tab-page", `
<div>
	<form action="" autocomplete="off">
		<div class="userscript-popup-main-area">
		</div>
		<div class="userscript-popup-button-bar">
			<button class="button userscript-popup-close-button">
				<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
				<span>Schließen</span>
			</button>
		</div>
	</form>
</div>
`);

JavaForum.registerTemplate("userscript-message-action", `
<a href="" class="actionBar-action userscript-message-action-text-target" rel="nofollow"></a>
`);

JavaForum.registerTemplate("userscript-popup-tab", `
<a class="tabs-tab userscript-popup-tab" href="" tabindex="0"></a>
`);

JavaForum.registerTemplate("userscript-popup-tab-page", `
<div class="userscript-popup-tab-page"></div>
`);

JavaForum.registerMenuTabPage("preferences", "Einstellungen", "Die Einstellungen für dieses UserScript.", 9900, "userscript-menu-popup-preferences-tab-page", function(tabPageElement) {
	for (let preference of JavaForum.preferences) {
		let preferenceContainer = document.createElement("div");
		preferenceContainer.classList.add("userscript-preference-row");
		
		let labelElement = document.createElement("label");
		labelElement.classList.add("userscript-preference-label");
		labelElement.setAttribute("for", "preference-" + preference.name);
		labelElement.innerText = preference.text;
		labelElement.title = preference.tooltip;
		
		preferenceContainer.appendChild(labelElement);
		
		if (preference.type === "boolean") {
			let inputElement = document.createElement("input");
			inputElement.classList.add("userscript-preference-input");
			inputElement.checked = JavaForum.getPreference(preference.name);
			inputElement.id = "preference-" + preference.name;
			inputElement.type = "checkbox";
			inputElement.title = preference.tooltip;
			inputElement.addEventListener("change", function(changeEvent) {
				JavaForum.setPreference(preference.name, inputElement.checked);
			});
			
			preferenceContainer.appendChild(inputElement);
		} else if (preference.type === "float") {
			let inputElement = document.createElement("input");
			inputElement.classList.add("userscript-preference-input");
			inputElement.id = "preference-" + preference.name;
			inputElement.max = "1.00";
			inputElement.min = "0.00";
			inputElement.size = "6";
			inputElement.step = "0.01";
			inputElement.type = "number";
			inputElement.title = preference.tooltip;
			inputElement.value = JavaForum.getPreference(preference.name);
			inputElement.addEventListener("change", function(changeEvent) {
				JavaForum.setPreference(preference.name, inputElement.value);
			});
			
			preferenceContainer.appendChild(inputElement);
		} else if (preference.type === "int") {
			let inputElement = document.createElement("input");
			inputElement.classList.add("userscript-preference-input");
			inputElement.id = "preference-" + preference.name;
			inputElement.max = "999999";
			inputElement.min = "0";
			inputElement.size = "6";
			inputElement.step = "1";
			inputElement.type = "number";
			inputElement.title = preference.tooltip;
			inputElement.value = JavaForum.getPreference(preference.name);
			inputElement.addEventListener("change", function(changeEvent) {
				JavaForum.setPreference(preference.name, inputElement.value);
			});
			
			preferenceContainer.appendChild(inputElement);
		} else if (preference.type === "string") {
			let inputElement = document.createElement("input");
			inputElement.classList.add("userscript-preference-input");
			inputElement.id = "preference-" + preference.name;
			inputElement.type = "text";
			inputElement.title = preference.tooltip;
			inputElement.value = JavaForum.getPreference(preference.name);
			inputElement.addEventListener("change", function(changeEvent) {
				JavaForum.setPreference(preference.name, inputElement.value);
			});
			
			preferenceContainer.appendChild(inputElement);
		} else {
			JavaForum.error("Encountered unknown preference type <%s> on preference <%s>.",
					preference.type,
					preference.name);
		}
		
		tabPageElement.querySelector(".userscript-popup-main-area").appendChild(preferenceContainer);
	}
});

JavaForum.registerMenuTabPage("data", "Daten", "Die Roh-Daten welches das UserScript speichert.", 10000, "userscript-menu-popup-data-tab-page", function(tabPageElement) {
	tabPageElement.querySelector("#userscript-menu-popup-data-editor").value = Util.toJson(Userscript.getData());
	
	tabPageElement.querySelector("#userscript-menu-popup-data-delete-button").addEventListener("click", function(mouseEvent) {
		mouseEvent.preventDefault();
		
		if (confirm("Sollen wirklich alle Daten des Skripts gelöscht werden?")) {
			Userscript.clearData();
			
			if (confirm("Daten wurden alle gelöscht, soll die Seite jetzt neu geladen werden?")) {
				location.reload();
			}
		}
	});
	
	tabPageElement.querySelector("#userscript-menu-popup-data-save-button").addEventListener("click", function(mouseEvent) {
		mouseEvent.preventDefault();
		
		let data = JSON.parse(tabPageElement.querySelector("#userscript-menu-popup-data-editor").value);
		
		for (let key in data) {
			GM_setValue(key, data[key]);
		}
		
		if (confirm("Daten wurden importiert, soll die Seite jetzt neu geladen werden?")) {
			location.reload();
		}
	});
});

JavaForum.registerMemberTooltipListener(JavaForum.extendMemberTooltip);
JavaForum.registerMessageListener(JavaForum.extendMessage);

JavaForum.registerElementProcessor(".memberTooltip", JavaForum.invokeMemberTooltipListeners);
JavaForum.registerElementProcessor(".message", JavaForum.invokeMessageListeners);

new MutationObserver(function(mutationsList, observer) {
	for (let mutation of mutationsList) {
		if (mutation.type === 'childList') {
			for (let addedNode of mutation.addedNodes) {
				JavaForum.invokeElementProcessors(addedNode);
			}
		} else if (mutation.type === 'attributes') {
		}
	}
}).observe(document.body, {
	attributes: false,
	childList: true,
	subtree: true
});
// =============================================================================


let Tags = {
	cleanupTags: function(tags) {
		if (tags === undefined || tags === null) {
			return [];
		}
		
		let cleanedupTags = [];
		
		for (let tag of tags) {
			if (Tags.isTagUsable(tag)) {
				cleanedupTags.push(Util.simplifyObject(tag));
			}
		}
		
		return cleanedupTags;
	},
	
	createAddButton: function() {
		let addButton = JavaForum.createFromTemplate("tags-tag-add-button");
		
		addButton.addEventListener("click", (mouseEvent) => {
			let addButton = mouseEvent.currentTarget;
			let tagsContainer = addButton.parentNode;
			
			Tags.showTagsEditor(null, tagsContainer, addButton);
		});
		
		return addButton;
	},
	
	createMinifiedTagElement: function(tag) {
		let tagElement = document.createElement("div");
		tagElement.classList.toggle("tag-fontawesome", tag.fontawesome);
		tagElement.classList.add("tag-minified");
		tagElement.style.backgroundColor = tag.color;
		tagElement.style.borderColor = Colors.blend("#cccccc", tag.color, 0.3);
		tagElement.title = tag.text;
		
		return tagElement;
	},
	
	createMinifiedTagsContainer: function(prefix, id, name) {
		let tagsContainer = document.createElement("div");
		tagsContainer.classList.add("tags-minified");
		tagsContainer.dataset.id = id;
		tagsContainer.dataset.name = name;
		tagsContainer.dataset.prefix = prefix;
		
		return tagsContainer;
	},
	
	createTagElement: function(tag) {
		let tagElement = document.createElement("div");
		tagElement.classList.add("tag");
		tagElement.classList.toggle("tag-fontawesome", tag.fontawesome);
		tagElement.innerHTML = tag.text;
		tagElement.style.backgroundColor = tag.color;
		
		if (tag.notes !== undefined && tag.notes !== null) {
			tagElement.title = tag.notes;
		}
		
		if (JavaForum.getPreference("inverted-color-for-text")) {
			tagElement.style.color = Colors.invert(tag.color);
		} else {
			tagElement.style.color = Colors.blackWhite(Colors.invert(tag.color));
		}
		
		tagElement.style.borderColor = Colors.blend("#cccccc", tag.color, 0.3);
		
		tagElement.addEventListener("click", (mouseEvent) => {
			let tagElement = mouseEvent.currentTarget;
			let userTagsContainer = tagElement.parentNode;
			
			Tags.showTagsEditor(tag, userTagsContainer, tagElement);
		});
		
		return tagElement;
	},
	
	createTagsContainer: function(prefix, id, name) {
		let tagsContainer = document.createElement("div");
		tagsContainer.classList.add("tags");
		tagsContainer.classList.add(prefix + "-tags");
		tagsContainer.dataset.id = id;
		tagsContainer.dataset.name = name;
		tagsContainer.dataset.prefix = prefix;
		
		return tagsContainer;
	},
	
	isTagUsable: function(tag) {
		return tag !== undefined
				&& tag !== null
				&& typeof tag === "object"
				&& tag.text !== undefined
				&& tag.text !== null;
	},
	
	loadData: function(prefix, id, name) {
		let key = prefix + "-" + id;
		let data = Util.simplifyObject(GM_getValue(key, null));
		
		if (data !== null) {
			data.tags = Tags.cleanupTags(data.tags);
		} else {
			data = {
					name: name,
					tags: []};
		}
		
		return data;
	},
	
	loadDataForTagsContainer: function(tagsContainer) {
		let id = tagsContainer.dataset.id;
		let name = tagsContainer.dataset.name;
		let prefix = tagsContainer.dataset.prefix;
		
		return Tags.loadData(prefix, id, name);
	},
	
	makeTagEffectsTarget: function(prefix, id, element, conditionName, effects) {
		element.classList.add("tags-tag-effects-target");
		element.dataset.tagsEffects = JSON.stringify(effects);
		if (conditionName !== null) {
			element.dataset.tagsEffectsCondition = conditionName;
		}
		element.dataset.tagsId = id;
		element.dataset.tagsPrefix = prefix;
		
		Tags.updateTagEffectsTarget(element);
	},
	
	showTagsEditor: function(currentTag, tagsContainer, parentElement) {
		let tagsEditor = JavaForum.togglePopup("tags-editor", parentElement, "absolute");
		
		let sharedContainer = {
			tagIndex: Array.prototype.indexOf.call(tagsContainer.children, parentElement)
		};
		
		tagsEditor.querySelector("#tags-editor-move-left.button").addEventListener("click", function(mouseEvent) {
			mouseEvent.preventDefault();
			
			let data = Tags.loadDataForTagsContainer(tagsContainer);
			
			if (sharedContainer.tagIndex > 0) {
				let swappedTag = data.tags[sharedContainer.tagIndex - 1];
				data.tags[sharedContainer.tagIndex - 1] = currentTag;
				data.tags[sharedContainer.tagIndex] = swappedTag;
				
				Tags.saveDataForTagsContainer(tagsContainer, data);
				
				sharedContainer.tagIndex = sharedContainer.tagIndex - 1;
			}
		});
		tagsEditor.querySelector("#tags-editor-move-right.button").addEventListener("click", function(mouseEvent) {
			mouseEvent.preventDefault();
			
			let data = Tags.loadDataForTagsContainer(tagsContainer);
			
			if (sharedContainer.tagIndex >= 0 && sharedContainer.tagIndex < userData.tags.length - 1) {
				let swappedTag = data.tags[sharedContainer.tagIndex + 1];
				data.tags[sharedContainer.tagIndex + 1] = currentTag;
				data.tags[sharedContainer.tagIndex] = swappedTag;
				
				Tags.saveDataForTagsContainer(tagsContainer, data);
				
				sharedContainer.tagIndex = sharedContainer.tagIndex + 1;
			}
		});
		tagsEditor.querySelector("#tags-editor-input-fontawesome").addEventListener("click", function(mouseEvent) {
			mouseEvent.preventDefault();
			
			tagsEditor.querySelector("#tags-editor-input-fontawesome").classList.toggle("tags-button-selected");
		});
		tagsEditor.querySelector("#tags-editor-delete.button").addEventListener("click", function(mouseEvent) {
			mouseEvent.preventDefault();
			
			if (!JavaForum.getPreference("confirm-deletion") || confirm("Soll dieser Tag <" + currentTag.text + "> wirklich gelöscht werden?")) {
				let data = Tags.loadDataForTagsContainer(tagsContainer);
				
				if (sharedContainer.tagIndex >= 0 && sharedContainer.tagIndex < data.tags.length) {
					data.tags.splice(sharedContainer.tagIndex, 1);
				}
				
				Tags.saveDataForTagsContainer(tagsContainer, data);
				
				JavaForum.closePopup("tags-editor");
			}
		});
		tagsEditor.querySelector("#tags-editor-save.button").addEventListener("click", function(mouseEvent) {
			mouseEvent.preventDefault();
			
			let text = tagsEditor.querySelector("#tags-editor-input-text").value;
			let color = tagsEditor.querySelector("#tags-editor-input-color").value.toLowerCase();
			let notes = tagsEditor.querySelector("#tags-editor-input-notes").value;
			let fontawesome = tagsEditor.querySelector("#tags-editor-input-fontawesome").classList.contains("tags-button-selected");
			
			let highlightColor = tagsEditor.querySelector("#tags-editor-input-highlight-color").value.toLowerCase();
			let blur = tagsEditor.querySelector("#tags-editor-input-blur").classList.contains("tags-button-selected");
			let translucent = tagsEditor.querySelector("#tags-editor-input-translucent").classList.contains("tags-button-selected");
			let revealOnHover = tagsEditor.querySelector("#tags-editor-input-reveal-on-hover").classList.contains("tags-button-selected");
			
			if (text !== undefined && text !== null && text !== "") {
				let data = Tags.loadDataForTagsContainer(tagsContainer);
				let tag = {};
				
				if (currentTag !== null) {
					if (sharedContainer.tagIndex >= 0 && sharedContainer.tagIndex < data.tags.length) {
						tag = data.tags[sharedContainer.tagIndex];
					} else {
						data.tags.push(tag);
					}
				} else {
					data.tags.push(tag);
				}
				
				tag.color = color;
				tag.fontawesome = fontawesome;
				tag.notes = notes;
				tag.text = text;
				
				tag.effects = {};
				tag.effects.blur = blur;
				tag.effects.color = highlightColor;
				tag.effects.translucent = translucent;
				tag.effects.revealOnHover = revealOnHover;
				
				Tags.saveDataForTagsContainer(tagsContainer, data);
				
				JavaForum.closePopup("tags-editor");
			}
		});
		
		tagsEditor.querySelector("#tags-editor-input-blur").addEventListener("click", function(mouseEvent) {
			mouseEvent.preventDefault();
			
			tagsEditor.querySelector("#tags-editor-input-blur").classList.toggle("tags-button-selected");
		});
		tagsEditor.querySelector("#tags-editor-input-translucent").addEventListener("click", function(mouseEvent) {
			mouseEvent.preventDefault();
			
			tagsEditor.querySelector("#tags-editor-input-translucent").classList.toggle("tags-button-selected");
		});
		tagsEditor.querySelector("#tags-editor-input-reveal-on-hover").addEventListener("click", function(mouseEvent) {
			mouseEvent.preventDefault();
			
			tagsEditor.querySelector("#tags-editor-input-reveal-on-hover").classList.toggle("tags-button-selected");
		});
		
		if (currentTag !== null) {
			tagsEditor.querySelector("#tags-editor-input-text").value = currentTag.text;
			tagsEditor.querySelector("#tags-editor-input-color").value = currentTag.color;
			tagsEditor.querySelector("#tags-editor-input-fontawesome").classList.toggle("tags-button-selected", !!currentTag.fontawesome);
			
			if (currentTag.notes !== undefined && currentTag.notes !== null) {
				tagsEditor.querySelector("#tags-editor-input-notes").value = currentTag.notes;
			}
			
			tagsEditor.querySelector("#tags-editor-input-blur").classList.toggle("tags-button-selected", !!currentTag.effects.blur);
			tagsEditor.querySelector("#tags-editor-input-highlight-color").disabled = true;
			tagsEditor.querySelector("#tags-editor-input-highlight-color").value = currentTag.effects.color;
			tagsEditor.querySelector("#tags-editor-input-highlight-color").disabled = false;
			tagsEditor.querySelector("#tags-editor-input-translucent").classList.toggle("tags-button-selected", !!currentTag.effects.translucent);
			tagsEditor.querySelector("#tags-editor-input-reveal-on-hover").classList.toggle("tags-button-selected", !!currentTag.effects.revealOnHover);
		} else {
			tagsEditor.querySelector("#tags-editor-input-highlight-color").disabled = true;
			tagsEditor.querySelector("#tags-editor-input-highlight-color").value = "#ffffff";
			tagsEditor.querySelector("#tags-editor-input-highlight-color").disabled = false;
			
			tagsEditor.querySelector("#tags-editor-delete").classList.add("userscript-hidden");
			tagsEditor.querySelector("#tags-editor-move-buttons").classList.add("userscript-hidden");
		}
		
		let templateTagsContainer = Tags.createTagsContainer(tagsContainer.dataset.prefix, "templates");
		
		tagsEditor.querySelector("#tags-editor-templates").appendChild(templateTagsContainer);
		
		Tags.updateTemplateTagsContainer(templateTagsContainer, tagsContainer.dataset.prefix, tagsContainer.dataset.id, tagsContainer.dataset.name);
	},
	
	saveData: function(prefix, id, data) {
		let key = prefix + "-" + id;
		
		if (data === undefined || data === null) {
			GM_deleteValue(key);
			
			return;
		}
		
		let simplifiedData = Util.simplifyObject(data);
		simplifiedData.tags = Tags.cleanupTags(data.tags);
		
		if (simplifiedData.tags.length > 0) {
			GM_setValue(key, simplifiedData);
		} else {
			GM_deleteValue(key);
		}
		
		Tags.updateTagsContainersOf(prefix, id);
		Tags.updateTagEffectsTargetsOf(prefix, id);
	},
	
	saveDataForTagsContainer: function(tagsContainer, data) {
		let id = tagsContainer.dataset.id;
		let name = tagsContainer.dataset.name;
		let prefix = tagsContainer.dataset.prefix;
		
		data.name = name;
		
		Tags.saveData(prefix, id, data);
	},
	
	updateMinifiedTagsContainer: function(tagsContainer) {
		tagsContainer.innerHTML = "";
		
		if (tagsContainer.dataset.condition !== undefined
				&& tagsContainer.dataset.condition !== null
				&& !JavaForum.getPreference("show-tags-in-" + tagsContainer.dataset.condition)) {
			return;
		}
		
		let data = Tags.loadDataForTagsContainer(tagsContainer);
		
		for (let tag of data.tags) {
			tagsContainer.appendChild(Tags.createMinifiedTagElement(tag));
		}
	},
	
	updateTagEffectsTarget: function(tagEffectsTargetElement) {
		tagEffectsTargetElement.style.setProperty("--tags-effect-color", "#ffffff");
		
		if (tagEffectsTargetElement.dataset.tagsEffectsCondition !== undefined
				&& tagEffectsTargetElement.dataset.tagsEffectsCondition !== null
				&& !JavaForum.getPreference("effects-affect-" + tagEffectsTargetElement.dataset.tagsEffectsCondition)) {
			// Do not apply effects.
			return;
		};
		
		if (tagEffectsTargetElement.dataset.tagsEffects === undefined || tagEffectsTargetElement.dataset.tagsEffects === null) {
			return;
		}
		
		let tagsEffects = JSON.parse(tagEffectsTargetElement.dataset.tagsEffects);
		
		if (tagsEffects === undefined
				|| tagsEffects === null
				|| tagsEffects.length === 0) {
			return;
		}
		
		tagEffectsTargetElement.classList.remove("tag-effect-blur");
		tagEffectsTargetElement.classList.remove("tag-effect-translucent");
		tagEffectsTargetElement.classList.remove("tag-effect-revealable");
		tagEffectsTargetElement.style.setProperty("--blur-strength", null);
		tagEffectsTargetElement.style.setProperty("--translucent-strength", null);
		
		if (tagsEffects.includes("background-color")) {
			tagEffectsTargetElement.style.backgroundColor = null;
		}
		if (tagsEffects.includes("border")) {
			tagEffectsTargetElement.style.border = null;
		}
		if (tagsEffects.includes("color")) {
			tagEffectsTargetElement.style.color = null;
		}
		
		let currentStyle = window.getComputedStyle(tagEffectsTargetElement);
		let currentBackgroundColor = Colors.rgbToHex(currentStyle.getPropertyValue("background-color"));
		let currentColor = Colors.rgbToHex(currentStyle.getPropertyValue("color"));
		
		let id = tagEffectsTargetElement.dataset.tagsId;
		let prefix = tagEffectsTargetElement.dataset.tagsPrefix;
		
		let data = Tags.loadData(prefix, id, null);
		
		if (data.tags.length === 0) {
			return;
		}
		
		for (let tag of data.tags) {
			if (tag.effects.blur && tagsEffects.includes("blur")) {
				tagEffectsTargetElement.style.setProperty("--blur-strength", JavaForum.getPreference("effects-blur-strength") + "em");
				tagEffectsTargetElement.classList.add("tag-effect-blur");
				
				if (tag.effects.revealOnHover) {
					tagEffectsTargetElement.classList.add("tag-effect-revealable");
				}
			}
			
			if (tag.effects.translucent && tagsEffects.includes("translucent")) {
				tagEffectsTargetElement.style.setProperty("--translucent-strength", 1.0 - JavaForum.getPreference("effects-translucent-strength"));
				tagEffectsTargetElement.classList.add("tag-effect-translucent");
				
				if (tag.effects.revealOnHover) {
					tagEffectsTargetElement.classList.add("tag-effect-revealable");
				}
			}
			
			if (tag.effects.color && tag.effects.color !== "#ffffff") {
				let color = tag.effects.color;
				
				if (tagsEffects.includes("background-color")) {
					let blendStrength = JavaForum.getPreference("effects-" + tagEffectsTargetElement.dataset.tagsEffectsCondition + "-color-blend-strength");
					
					if (blendStrength !== null) {
						let blendedColor = Colors.blend(currentBackgroundColor, color, blendStrength);
						
						tagEffectsTargetElement.style.backgroundColor = blendedColor;
						tagEffectsTargetElement.style.setProperty("--tags-effect-color", blendedColor);
					} else {
						tagEffectsTargetElement.style.backgroundColor = color;
						tagEffectsTargetElement.style.setProperty("--tags-effect-color", color);
					}
				}
				
				if (tagsEffects.includes("border")) {
					tagEffectsTargetElement.style.border = "1px solid " + color;
				}
				
				if (tagsEffects.includes("color")) {
					let blendStrength = JavaForum.getPreference("effects-" + tagEffectsTargetElement.dataset.tagsEffectsCondition + "-color-blend-strength");
					
					if (blendStrength !== null) {
						let blendedColor = Colors.blend(currentColor, color, blendStrength);
						
						tagEffectsTargetElement.style.color = blendedColor;
					} else {
						tagEffectsTargetElement.style.color = color;
					}
				}
				
				if (tagsEffects.includes("tags-effect-color")) {
					let blendStrength = JavaForum.getPreference("effects-" + tagEffectsTargetElement.dataset.tagsEffectsCondition + "-color-blend-strength");
					
					if (blendStrength !== null) {
						let blendedColor = Colors.blend(currentBackgroundColor, color, blendStrength);
						
						tagEffectsTargetElement.style.setProperty("--tags-effect-color", blendedColor);
					} else {
						tagEffectsTargetElement.style.setProperty("--tags-effect-color", color);
					}
				}
			}
		}
	},
	
	updateTagEffectsTargets: function() {
		for (let tagEffectsTargetElement of document.body.querySelectorAll(".tags-tag-effects-target")) {
			Tags.updateTagEffectsTarget(tagEffectsTargetElement);
		}
	},
	
	updateTagEffectsTargetsOf: function(prefix, id) {
		for (let tagEffectsTargetElement of document.body.querySelectorAll(".tags-tag-effects-target")) {
			if (tagEffectsTargetElement.dataset.tagsPrefix === prefix && tagEffectsTargetElement.dataset.tagsId === id) {
				Tags.updateTagEffectsTarget(tagEffectsTargetElement);
			}
		}
	},
	
	updateTagsContainer: function(tagsContainer) {
		tagsContainer.innerHTML = "";
		
		let data = Tags.loadDataForTagsContainer(tagsContainer);
		
		for (let tag of data.tags) {
			tagsContainer.appendChild(Tags.createTagElement(tag));
		}
		
		tagsContainer.appendChild(Tags.createAddButton());
	},
	
	updateTagsContainers: function() {
		for (let tagsContainer of document.querySelectorAll(".tags")) {
			Tags.updateTagsContainer(tagsContainer);
		}
		
		for (let minifiedTagsContainer of document.querySelectorAll(".tags-minified")) {
			Tags.updateMinifiedTagsContainer(minifiedTagsContainer);
		}
	},
	
	updateTagsContainersOf: function(prefix, id) {
		for (let tagsContainer of document.querySelectorAll(".tags")) {
			if (tagsContainer.dataset.prefix === prefix && tagsContainer.dataset.id === id) {
				Tags.updateTagsContainer(tagsContainer);
			}
		}
		
		for (let minifiedTagsContainer of document.querySelectorAll(".tags-minified")) {
			if (minifiedTagsContainer.dataset.prefix === prefix && minifiedTagsContainer.dataset.id === id) {
				Tags.updateMinifiedTagsContainer(minifiedTagsContainer);
			}
		}
	},
	
	updateTemplateTagsContainer(tagsContainer, targetPrefix, targetId, targetName) {
		tagsContainer.innerHTML = "";
		
		for (let tag of Tags.loadDataForTagsContainer(tagsContainer).tags) {
			let tagElement = (Tags.createTagElement(tag)).cloneNode(true);
			tagElement.addEventListener("click", function(mouseEvent) {
				mouseEvent.preventDefault();
				
				let data = Tags.loadData(targetPrefix, targetId, targetName);
				
				data.tags.push(tag);
				
				Tags.saveData(targetPrefix, targetId, data);
			});
			
			tagsContainer.appendChild(tagElement);
		}
	}
};

let ForumTags = {
	extractForumId: function(url) {
		if (url === undefined || url === null) {
			return null;
		}
		
		if (url.indexOf("/forum/") < 0) {
			return null;
		}
		
		let dotIndex = url.lastIndexOf(".");
		
		if (dotIndex < 0) {
			return null;
		}
		
		let forumId = url.substring(dotIndex + 1);
		
		let slashIndex = forumId.indexOf("/");
		
		if (slashIndex >= 0) {
			forumId = forumId.substring(0, slashIndex);
		}
		
		return forumId;
	},
	
	processLinkElement: function(linkElement, conditionName) {
		if (linkElement.parentNode.classList.contains("pageNav-page")) {
			return;
		}
		
		let forumId = ForumTags.extractForumId(linkElement.href);
		let forumName = linkElement.innerText;
		
		if (forumId !== null) {
			let tagsContainer = Tags.createMinifiedTagsContainer("forum", forumId, forumName);
			tagsContainer.dataset.condition = conditionName;
			tagsContainer.style.display = "inline-block";
			tagsContainer.style.paddingLeft = "0.5em";
			
			linkElement.parentNode.appendChild(tagsContainer);
			
			Tags.updateMinifiedTagsContainer(tagsContainer);
			
			Tags.makeTagEffectsTarget("forum", forumId, linkElement, "forum-titles", [ "blur", "color", "translucent" ]);
		}
	},
	
	processLinkElementInOverview: function(linkElement, conditionName) {
		let forumId = ForumTags.extractForumId(linkElement.href);
		let forumName = linkElement.innerText;
		
		if (forumId !== null) {
			let tagsContainer = Tags.createMinifiedTagsContainer("forum", forumId, forumName);
			tagsContainer.dataset.condition = conditionName;
			tagsContainer.style.display = "inline-block";
			tagsContainer.style.paddingLeft = "0.5em";
			
			linkElement.parentNode.appendChild(tagsContainer);
			
			Tags.updateMinifiedTagsContainer(tagsContainer);
			
			Tags.makeTagEffectsTarget("forum", forumId, linkElement, "forum-titles", [ "blur", "color", "translucent" ]);
		}
	},
	
	processPage: function() {
		let forumId = ForumTags.extractForumId(window.location.href);
		
		if (forumId !== null) {
			let titleElement = document.querySelector("div.p-title");
			
			if (titleElement !== null) {
				let titleValueElement = titleElement.querySelector(".p-title-value");
				
				Tags.makeTagEffectsTarget("forum", forumId, titleValueElement, "forum-titles", [ "blur", "color", "translucent" ]);
				
				let tagsContainer = Tags.createTagsContainer("forum", forumId, titleValueElement.innerText);
				tagsContainer.style.flex = "1";
				tagsContainer.style.paddingLeft = "0.5em";
				
				Tags.updateTagsContainer(tagsContainer);
				
				titleElement.insertBefore(tagsContainer, titleValueElement.nextSibling);
			}
		}
	}
};

let ThreadTags = {
	extractThreadId: function(url) {
		if (url === undefined || url === null) {
			return null;
		}
		
		if (url.indexOf("/thema/") < 0) {
			return null;
		}
		
		let dotIndex = url.lastIndexOf(".");
		
		if (dotIndex < 0) {
			return null;
		}
		
		let threadId = url.substring(dotIndex + 1);
		
		let slashIndex = threadId.indexOf("/");
		
		if (slashIndex >= 0) {
			threadId = threadId.substring(0, slashIndex);
		}
		
		return threadId;
	},
	
	processPage: function() {
		let threadId = ThreadTags.extractThreadId(window.location.href);
		
		if (threadId !== null) {
			let titleElement = document.querySelector("div.p-title");
			
			if (titleElement !== null) {
				let titleValueElement = titleElement.querySelector(".p-title-value");
				
				Tags.makeTagEffectsTarget("thread", threadId, titleValueElement, "thread-titles", [ "blur", "color", "translucent" ]);
				
				let tagsContainer = Tags.createTagsContainer("thread", threadId, titleValueElement.innerText);
				tagsContainer.style.flex = "1";
				tagsContainer.style.paddingLeft = "0.5em";
				
				Tags.updateTagsContainer(tagsContainer);
				
				titleElement.appendChild(tagsContainer);
			}
		}
	},
	
	processTitleElement: function(element) {
		let threadLink = element.querySelector("a");
		
		if (threadLink !== null) {
			let threadId = ThreadTags.extractThreadId(threadLink.href);
			
			if(threadId !== null) {
				let parentElement = threadLink.parentElement;
				parentElement.style.display = "flex";
				
				let tagsContainer = Tags.createTagsContainer("thread", threadId, threadLink.innerText);
				tagsContainer.style.display = "inline-block";
				tagsContainer.style.paddingLeft = "0.5em";
				
				Tags.updateTagsContainer(tagsContainer);
				
				parentElement.appendChild(tagsContainer);
				
				Tags.makeTagEffectsTarget("thread", threadId, element.querySelector("a"), "thread-titles", [ "blur", "color", "translucent" ]);
			}
		}
	},
	
	processTitleInListContainer: function(titleElement, conditionName) {
		let threadId = ThreadTags.extractThreadId(titleElement.href);
		let threadName = titleElement.innerText;
		
		if (threadId !== null) {
			let tagsContainer = Tags.createMinifiedTagsContainer("thread", threadId, threadName);
			tagsContainer.dataset.condition = conditionName;
			tagsContainer.style.display = "inline-block";
			tagsContainer.style.paddingLeft = "0.5em";
			
			titleElement.parentNode.appendChild(tagsContainer);
			
			Tags.updateMinifiedTagsContainer(tagsContainer);
			
			Tags.makeTagEffectsTarget("thread", threadId, titleElement, "thread-titles", [ "blur", "color", "translucent" ]);
		}
	},
};

let UserTags = {
	processArticleElement: function(articleElement) {
		let userId = null;
		let userName = null;
		
		let usernameElement = articleElement.querySelector("a.username");
		
		if (usernameElement !== undefined && usernameElement !== null) {
			userId = usernameElement.dataset.userId;
			userName = usernameElement.innerText;
		} else {
			let avatarElement = articleElement.querySelector("a.avatar");
			
			if (avatarElement !== undefined && avatarElement !== null) {
				userId = avatarElement.dataset.userId;
				userName = avatarElement.querySelector("img").alt;
			}
		}
		
		if (userId === null) {
			return;
		}
		
		for (let messageCellUserElement of articleElement.querySelectorAll(".message-cell--user")) {
			let tagsContainer = messageCellUserElement.querySelector(".tags");
			
			if (tagsContainer === undefined || tagsContainer === null) {
				tagsContainer = Tags.createTagsContainer("user", userId, userName);
				
				messageCellUserElement.appendChild(tagsContainer);
			}
			
			Tags.updateTagsContainer(tagsContainer);
		}
		
		for (let messageCellUserElement of articleElement.querySelectorAll(".message-cell--user")) {
			Tags.makeTagEffectsTarget("user", userId, messageCellUserElement, "article-background", [ "background-color" ]);
		}
		
		for (let messageCellMainElement of articleElement.querySelectorAll(".message-cell--main")) {
			Tags.makeTagEffectsTarget("user", userId, messageCellMainElement, "article-background", [ "background-color" ]);
		}
		
		for (let messageUserArrowElement of articleElement.querySelectorAll(".message-userArrow")) {
			Tags.makeTagEffectsTarget("user", userId, messageUserArrowElement, "article-background", [ "tags-effect-color" ]);
		}
		
		for (let messageContentElement of articleElement.querySelectorAll(".message-content")) {
			Tags.makeTagEffectsTarget("user", userId, messageContentElement, null, [ "blur", "translucent" ]);
		}
	},
	
	processAvatarElement: function(avatarElement) {
		let userId = avatarElement.dataset.userId;
		
		if (userId !== undefined && userId !== null) {
			Tags.makeTagEffectsTarget("user", userId, avatarElement, "avatars", [ "blur", "border", "translucent" ]);
		}
	},
	
	processAvatarInListContainer: function(containerElement, conditionName) {
		let avatarElement = containerElement.querySelector("a.avatar");
		
		if (avatarElement !== undefined && avatarElement !== null) {
			let tagsContainer = Tags.createMinifiedTagsContainer("user", avatarElement.dataset.userId, avatarElement.alt);
			tagsContainer.dataset.condition = conditionName;
			
			containerElement.appendChild(tagsContainer);
			
			Tags.updateMinifiedTagsContainer(tagsContainer);
		}
	},
	
	processUsernameElement: function(usernameElement) {
		let userId = usernameElement.dataset.userId;
		
		if (userId !== undefined && userId !== null) {
			usernameElement.classList.add("tags-tag-effects-target");
			usernameElement.dataset.tagsEffects = JSON.stringify([ "blur", "color", "translucent" ]);
			usernameElement.dataset.tagsEffectsCondition = "usernames";
			usernameElement.dataset.tagsId = userId;
			usernameElement.dataset.tagsPrefix = "user";
			
			Tags.updateTagEffectsTarget(usernameElement);
		}
	}
};

JavaForum.registerStylesheet(`
#tags-editor-delete {
	background-color: #ff6228;
	border-color: #ff875b #ff6127 #ff6127 #ff875b;
}

#tags-editor-delete:hover {
	background-color: #dd3e03;
	border-color: #e86535 #dd3e03 #dd3e03 #e86535;
}

#tags-editor-delete > .userscript-icon::after {
	content: "\\f2ed";
}

#tags-editor-input-highlight-color {
	min-height: 2.3em;
}

#tags-editor-input-notes {
	width: 100%;
}

#tags-editor-input-text {
	min-width: 15em;
}

#tags-editor-move-left > .userscript-icon::after {
	content: "\\f355";
}

#tags-editor-move-right > .userscript-icon::after {
	content: "\\f356";
}

#tags-editor-save {
	float: right;
}

#tags-editor-save > .userscript-icon::after {
	content: "\\f0c7";
}

#tags-thread-all {
	max-height: 20em;
	overflow-y: auto;
}

#tags-user-all {
	max-height: 20em;
	overflow-y: auto;
}

.memberTooltip .tags {
	padding: 6px;
}

.message-userArrow.tags-tag-effects-target::after {
	border-right-color: var(--tags-effect-color);
}

.tag {
	border: 1px solid #cccccc;
	border-radius: 4px;
	cursor: pointer;
	display: inline-block;
	font-size: 0.6em;
	margin: 1px 2px;
	padding: 1px 2px;
}

.tag-add-button {
	background-color: #47a7eb;
	border-color: #5eb2ed #309ce8 #309ce8 #5eb2ed;
	color: #ffffff;
	padding: 1px 6px;
}

.tag-add-button:hover {
	background-color: #2295e7;
}

.tag-add-button  > .userscript-icon {
	vertical-align: unset;
}

.tag-add-button  > .userscript-icon::after {
	content: "\\2b";
}

.tag-effect-blur {
	filter: blur(var(--blur-strength));
}

.tag-effect-revealable:hover {
	filter: unset;
	opacity: unset;
}

.tag-effect-translucent {
	opacity: var(--translucent-strength);
}

.tag-fontawesome {
	font-family: "Font Awesome 5 Pro";
}

.tag-minified {
	border: 1px solid #cccccc;
	border-radius: 100%;
	display: inline-block;
	font-size: 0.6em;
	height: 1em;
	margin: 0;
	padding: 0;
	width: 1em;
}

.tags {
	line-height: 1em;
}

.tags-button-selected {
	background-color: #cae9ff !important;
}

.tags-minified {
	line-height: 0.6em;
}
`);

JavaForum.registerTemplate("tags-editor", `
<div class="userscript-popup" id="tags-editor">
	<span class="menu-arrow" style="left: 50%;"></span>
	<form action="" autocomplete="off">
		<div class="userscript-popup-main-area" id="tags-editor-templates"></div>
		<hr class="userscript-popup-separator">
		<div class="userscript-popup-main-area">
			<div class="buttonGroup" id="tags-editor-move-buttons">
				<a class="button" href="" id="tags-editor-move-left" title="Tag nach links schieben.">
					<i aria-hidden="true" class="userscript-icon"></i>
					<span class="button-text">&#x200b;</span>
				</a>
				<a class="button" href="" id="tags-editor-move-right" title="Tag nach rechts schieben.">
					<i aria-hidden="true" class="userscript-icon"></i>
					<span class="button-text">&#x200b;</span>
				</a>
			</div>
			<input autocomplete="false" class="userscript-popup-focus-this" id="tags-editor-input-text" placeholder="Text des Tags..." type="text">
			<input autocomplete="false" id="tags-editor-input-color" type="color">
			<div class="buttonGroup">
				<a class="button--link button" href="" id="tags-editor-input-fontawesome" title="Ob Font Awesome als Schriftart für diesen Tag-Text verwendet wird.">
					<span class="button-text">Font Awesome</span>
				</a>
			</div>
		</div>
		<div class="userscript-popup-main-area">
			<textarea id="tags-editor-input-notes" placeholder="Notizen zu diesem Tag..."></textarea>
		</div>
		<div class="userscript-popup-main-area">
			<div class="buttonGroup">
				<input class="button button--link" id="tags-editor-input-highlight-color" title="Alle Beiträge des Benutzers werden in dieser Farbe dargestellt." type="color">
				<a class="button--link button" href="" id="tags-editor-input-translucent" title="Alle Beiträge des Benutzers werden (leicht) durchsichtig.">
					<span class="button-text">Durchsichtig</span>
				</a>
				<a class="button--link button" href="" id="tags-editor-input-blur" title="Alle Beiträge des Benutzers werden verwaschen dargestellt.">
					<span class="button-text">Weichzeichnen</span>
				</a>
			</div>
			<div class="buttonGroup">
				<a class="button--link button" href="" id="tags-editor-input-reveal-on-hover" title="Die Filter (Durchsichtig, Weichzeichnen) werden beim fahren mit der Maus über den Beitrag ausgesetzt.">
					<span class="button-text">Anzeigen unter Maus</span>
				</a>
			</div>
		</div>
		<div class="userscript-popup-button-bar">
			<button class="button userscript-popup-close-button" id="tags-editor-cancel">
				<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
				<span>Abbrechen</span>
			</button>
			<button class="button" id="tags-editor-delete">
				<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
				<span>Löschen</span>
			</button>
			<button class="button button--primary userscript-popup-apply-button" id="tags-editor-save">
				<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
				<span>Speichern</span>
			</button>
		</div>
	</form>
</div>
`);

JavaForum.registerTemplate("tags-member-tooltip-tags", `
<div></div>
`);

JavaForum.registerTemplate("tags-tag-add-button", `
<div class="tag tag-add-button"  title="Einen neuen Tag hinzufügen.">+</div>
`);

JavaForum.registerTemplate("tags-menu-popup-templates-tab-page", `
<div>
	<div class="userscript-popup-main-area" id="tags-forum-templates">
		<div><strong>Foren</strong></div>
	</div>
	<div class="userscript-popup-main-area" id="tags-thread-templates">
		<div><strong>Themen</strong></div>
	</div>
	<div class="userscript-popup-main-area" id="tags-user-templates">
		<div><strong>Benutzer</strong></div>
	</div>
	<div class="userscript-popup-button-bar">
		<button class="button userscript-popup-close-button">
			<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
			<span>Schließen</span>
		</button>
	</div>
</div>
`);

JavaForum.registerTemplate("tags-menu-popup-tags-tab-page", `
<div>
	<div class="userscript-popup-main-area">
		<div><strong>Foren</strong></div>
		<div id="tags-forum-all"></div>
	</div>
	<div class="userscript-popup-main-area">
		<div><strong>Themen</strong></div>
		<div id="tags-thread-all"></div>
	</div>
	<div class="userscript-popup-main-area">
		<div><strong>Benutzer</strong></div>
		<div id="tags-user-all"></div>
	</div>
	<div class="userscript-popup-button-bar">
		<button class="button userscript-popup-close-button">
			<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
			<span>Schließen</span>
		</button>
	</div>
</div>
`);

JavaForum.registerTemplate("tags-menu-popup-import-tab-page", `
<div>
	<div class="userscript-popup-main-area" id="tags-thread-all">
		<div>Daten von "User Tags" oder "User Soft-Block" importieren...</div>
	</div>
	<div class="userscript-popup-main-area" id="tags-user-all">
		<textarea class="userscript-popup-focus-this" id="userscript-menu-popup-import-editor"></textarea>
	</div>
	<div class="userscript-popup-button-bar">
		<button class="button userscript-popup-close-button">
			<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
			<span>Schließen</span>
		</button>
		<button class="button button--primary userscript-popup-apply-button" id="userscript-menu-popup-import-import-button">
			<i aria-hidden="true" class="userscript-icon userscript-icon-with-text"></i>
			<span>Importieren</span>
		</button>
	</div>
</div>
`);

JavaForum.registerPreference("confirm-deletion", "Löschen bestätigen", "Ob das löschen von Tags bestätigt werden muss.", "boolean", true, null);
JavaForum.registerPreference("inverted-color-for-text", "Invertierte Textfarbe", "Ob der Text Schwarz/Weiß sein soll, oder ob die invertierte Hintergrundfarbe verwendet wird.", "boolean", false, function(name, value) {
	Tags.updateTagsContainers();
});
JavaForum.registerPreference("show-tags-in-overview", "Tags in Übersichten", "Ob die Tags auch in Übersichten (unter den Avataren) angezeigt werden sollen.", "boolean", true, function(name, value) {
	Tags.updateTagsContainers();
});
JavaForum.registerPreference("show-tags-in-lists", "Tags in Listen", "Ob die Tags auch in Listen (unter den Avataren) angezeigt werden sollen.", "boolean", true, function(name, value) {
	Tags.updateTagsContainers();
});
JavaForum.registerPreference("show-tags-in-alerts", "Tags in Hinweisen", "Ob die Tags auch in Hinweisen angezeigt werden sollen.", "boolean", true, function(name, value) {
	Tags.updateTagsContainers();
});
JavaForum.registerPreference("effects-blur-strength", "Weichzeichnenstärke", "Wie stark der Weichzeichnungsfilter sein soll.", "float", 0.2, function(name, value) {
	Tags.updateTagEffectsTargets();
});
JavaForum.registerPreference("effects-translucent-strength", "Transparenz", "Wie stark der Transparenzfilter sein soll.", "float", 0.8, function(name, value) {
	Tags.updateTagEffectsTargets();
});
JavaForum.registerPreference("effects-affect-avatars", "Avatare", "Ob auch Avatare von den Effekten betroffen sind.", "boolean", true, function(name, value) {
	Tags.updateTagEffectsTargets();
});
JavaForum.registerPreference("effects-affect-article-background", "Beitrags-Hintergründe", "Ob auch Hintergründe von Beiträgen eingefärbt werden sollen.", "boolean", true, function(name, value) {
	Tags.updateTagEffectsTargets();
});
JavaForum.registerPreference("effects-article-background-color-blend-strength", "Beitrags-Hintergrund Farbstärke", "Wie stark der Hintergrund von Beiträgen eingefärbt werden soll.", "float", 0.2, function(name, value) {
	Tags.updateTagEffectsTargets();
});
JavaForum.registerPreference("effects-affect-usernames", "Benutzernamen", "Ob auch Benutzernamen von den Effekten betroffen sind.", "boolean", true, function(name, value) {
	Tags.updateTagEffectsTargets();
});
JavaForum.registerPreference("effects-usernames-color-blend-strength", "Benutzernamen Farbstärke", "Wie stark die Benutzernamen eingefärbt werden sollen.", "float", 0.6, function(name, value) {
	Tags.updateTagEffectsTargets();
});
JavaForum.registerPreference("effects-affect-forum-titles", "Foren-Titel", "Ob auch Foren-Titel von den Effekten betroffen sind.", "boolean", true, function(name, value) {
	Tags.updateTagEffectsTargets();
});
JavaForum.registerPreference("effects-forum-titles-color-blend-strength", "Forum-Titel Farbstärke", "Wie stark die Foren-Titel eingefärbt werden sollen.", "float", 0.6, function(name, value) {
	Tags.updateTagEffectsTargets();
});
JavaForum.registerPreference("effects-affect-thread-titles", "Themen-Titel", "Ob auch Themen-Titel von den Effekten betroffen sind.", "boolean", true, function(name, value) {
	Tags.updateTagEffectsTargets();
});
JavaForum.registerPreference("effects-thread-titles-color-blend-strength", "Themen-Titel Farbstärke", "Wie stark die Themen-Titel eingefärbt werden sollen.", "float", 0.6, function(name, value) {
	Tags.updateTagEffectsTargets();
});

JavaForum.registerMenu("tags", "User Tags", "f02c", 5000);
JavaForum.registerMenuTabPage("tags-templates", "Vorlagen", "Die Vorlagen für Tags.", 1, "tags-menu-popup-templates-tab-page", function(tabPageElement) {
	let forumTemplatesTagContainer = Tags.createTagsContainer("forum", "templates", "Forum-Templates");
	
	Tags.updateTagsContainer(forumTemplatesTagContainer);
	
	tabPageElement.querySelector("#tags-forum-templates").appendChild(forumTemplatesTagContainer);
	
	let threadTemplatesTagContainer = Tags.createTagsContainer("thread", "templates", "Thread-Templates");
	
	Tags.updateTagsContainer(threadTemplatesTagContainer);
	
	tabPageElement.querySelector("#tags-thread-templates").appendChild(threadTemplatesTagContainer);
	
	let userTemplatesTagContainer = Tags.createTagsContainer("user", "templates", "User-Templates");
	
	Tags.updateTagsContainer(userTemplatesTagContainer);
	
	tabPageElement.querySelector("#tags-user-templates").appendChild(userTemplatesTagContainer);
});
JavaForum.registerMenuTabPage("tags-tags", "Tags", "Alle Tags.", 2, "tags-menu-popup-tags-tab-page", function(tabPageElement) {
	for (let key of GM_listValues()) {
		if (key.startsWith("forum-") && !key.endsWith("-templates")) {
			let data = GM_getValue(key);
			
			let threadTemplatesTagContainer = Tags.createTagsContainer("forum", key.substring(6), data.name);
			threadTemplatesTagContainer.style.display = "inline-block";
			threadTemplatesTagContainer.style.paddingLeft = "0.5em";
			
			Tags.updateTagsContainer(threadTemplatesTagContainer);
			
			let link = document.createElement("a");
			link.href = "https://www.java-forum.org/forum/" + key.substring(6);
			link.innerText = data.name;
			
			tabPageElement.querySelector("#tags-forum-all").appendChild(link);
			tabPageElement.querySelector("#tags-forum-all").appendChild(threadTemplatesTagContainer);
			tabPageElement.querySelector("#tags-forum-all").appendChild(document.createElement("br"));
		} else if (key.startsWith("thread-") && !key.endsWith("-templates")) {
			let data = GM_getValue(key);
			
			let threadTemplatesTagContainer = Tags.createTagsContainer("thread", key.substring(7), data.name);
			threadTemplatesTagContainer.style.display = "inline-block";
			threadTemplatesTagContainer.style.paddingLeft = "0.5em";
			
			Tags.updateTagsContainer(threadTemplatesTagContainer);
			
			let link = document.createElement("a");
			link.href = "https://www.java-forum.org/thema/" + key.substring(7);
			link.innerText = data.name;
			
			tabPageElement.querySelector("#tags-thread-all").appendChild(link);
			tabPageElement.querySelector("#tags-thread-all").appendChild(threadTemplatesTagContainer);
			tabPageElement.querySelector("#tags-thread-all").appendChild(document.createElement("br"));
		} else if (key.startsWith("user-") && !key.endsWith("-templates")) {
			let data = GM_getValue(key);
			
			let threadTemplatesTagContainer = Tags.createTagsContainer("user", key.substring(5), data.name);
			threadTemplatesTagContainer.style.display = "inline-block";
			threadTemplatesTagContainer.style.paddingLeft = "0.5em";
			
			Tags.updateTagsContainer(threadTemplatesTagContainer);
			
			let link = document.createElement("a");
			link.href = "https://www.java-forum.org/mitglied/" + key.substring(5);
			link.innerText = data.name;
			
			tabPageElement.querySelector("#tags-user-all").appendChild(link);
			tabPageElement.querySelector("#tags-user-all").appendChild(threadTemplatesTagContainer);
			tabPageElement.querySelector("#tags-user-all").appendChild(document.createElement("br"));
		}
	}
});
JavaForum.registerMenuTabPage("tags-import", "Import", "Importieren von Daten.", 3, "tags-menu-popup-import-tab-page", function(tabPageElement) {
	tabPageElement.querySelector("#userscript-menu-popup-import-import-button").addEventListener("click", function(mouseEvent) {
		mouseEvent.preventDefault();
		
		let importData = JSON.parse(tabPageElement.querySelector("#userscript-menu-popup-import-editor").value);
		
		for (let key in importData) {
			if (key.startsWith("preference-")) {
				if (key === "preference-affect-avatars") {
					GM_setValue("preference-effects-affect-avatars", importData[key]);
				} else if (key === "preference-affect-usernames") {
					GM_setValue("preference-effects-affect-usernames", importData[key]);
				} else if (key === "preference-blur-strength") {
					GM_setValue("preference-effects-background-blend-strength", importData[key]);
				} else if (key === "preference-background-blend-strength") {
					GM_setValue("preference-effects-blur-strength", importData[key]);
				} else if (key === "preference-username-blend-strength") {
					GM_setValue("preference-effects-thread-titles-color-blend-strength", importData[key]);
					GM_setValue("preference-effects-usernames-color-blend-strength", importData[key]);
				} else if (key === "preference-translucent-strength") {
					GM_setValue("preference-effects-translucent-strength", importData[key]);
				} else {
					GM_setValue(key, importData[key]);
				}
			} else if (key.startsWith("soft-block-")) {
				let userId = key.substring(11);
				
				let data = Tags.loadData("user", userId, userId);
				
				let softBlockData = importData[key];
				
				let color = "#eeeeec";
				
				if (softBlockData.color !== undefined && softBlockData.color !== null) {
					color = softBlockData.color;
				}
				
				let tag = {};
				
				tag.color = color;
				tag.fontawesome = false;
				tag.notes = "Importiert von User Soft-Block.";
				tag.text = "Soft-Block";
				
				tag.effects = {};
				tag.effects.blur = !!softBlockData.blur;
				tag.effects.color = color;
				tag.effects.translucent = !!softBlockData.translucent;
				tag.effects.revealOnHover = !!softBlockData.revealOnHover;
				
				data.tags.push(tag);
				
				Tags.saveData("user", userId, data);
			} else if (key.startsWith("tags-")) {
				let userId = key.substring(5);
				
				let data = Tags.loadData("user", userId, userId);
				
				for (let importTag of importData[key]) {
					let tag = {};
					
					tag.color = importTag.color;
					tag.fontawesome = !!importTag.fontawesome;
					tag.notes = importTag.notes;
					tag.text = importTag.text;
					
					tag.effects = {};
					tag.effects.blur = false;
					tag.effects.color = "#ffffff";
					tag.effects.translucent = false;
					tag.effects.revealOnHover = false;
					
					data.tags.push(tag);
				}
				
				Tags.saveData("user", userId, data);
			}
		}
		
		if (confirm("Daten wurden importiert, soll die Seite jetzt neu geladen werden?")) {
			location.reload();
		}
	});
});

JavaForum.registerMemberTooltipGroup("tags", -100, "tags-member-tooltip-tags", function(memberTooltipGroupElement, userId, userName) {
	let tagsContainer = Tags.createTagsContainer("user", userId, userName);
	
	Tags.updateTagsContainer(tagsContainer);
	
	memberTooltipGroupElement.appendChild(tagsContainer);
});

JavaForum.registerElementProcessor("article.message.message--post", UserTags.processArticleElement);
JavaForum.registerElementProcessor("form.js-quickReply", UserTags.processArticleElement);

JavaForum.registerElementProcessor("div.structItem-title", ThreadTags.processTitleElement);

JavaForum.registerElementProcessor(".avatar", UserTags.processAvatarElement);
JavaForum.registerElementProcessor(".username", UserTags.processUsernameElement);

JavaForum.registerElementProcessor(".node-extra-icon", function(avatarElement) {
	UserTags.processAvatarInListContainer(avatarElement, "overview");
});
JavaForum.registerElementProcessor(".structItem-cell--icon", function(avatarElement) {
	UserTags.processAvatarInListContainer(avatarElement, "lists");
});

JavaForum.registerElementProcessor(".node-extra-title", function(titleElement) {
	ThreadTags.processTitleInListContainer(titleElement, "overview");
});

JavaForum.registerElementProcessor(".node-title a", function(linkElement) {
	ForumTags.processLinkElementInOverview(linkElement, "overview");
});
JavaForum.registerElementProcessor(".structItem-cell .structItem-minor  a", function(linkElement) {
	ForumTags.processLinkElement(linkElement, "lists");
});

ForumTags.processPage();
ThreadTags.processPage();
