Java-Forum.org UserScripts
==========================

UserScripts fuer [java-forum.org](https://www.java-forum.org). Die Skripte koennen sowohl mit [Greasemonkey](https://www.greasespot.net/), [Tampermonkey](https://www.tampermonkey.net/) als auch [Firemonkey](https://addons.mozilla.org/en-US/firefox/addon/firemonkey/) verwendet werden. Theoretisch sollte es mit jedem UserScript AddOn funktionieren.

Alle Skripte verwenden eine "Foren-Bibliothek" welche in den Kopf des Skripts kopiert ist. Diese Bibliothek soll eine einfache Basis stellen um es zu erlauben schnell und einfach UserScripts fuer das Forum zu schreiben. Bei Aenderungen in dieser Bibliothek ist diese in alle Skripte zu kopieren, und nach Bedarf die Skripte anzupassen an die Aenderungen. Leider unterstuetzten bestimmte UserScript AddOns nicht das einbinden anderer Skripte als Bibliotheken, und um die zusaetzliche (moeglicherweise fehlschlagende HTTP Anfrage) zu vermeiden wird diese Bibliothek einfach in die Skripte kopiert.


OpenAI
------

OpenAI ist eine simple OpenAI-Integration welche es erlaubt Beitraege direkt an OpenAI Modelle zu senden.

**Man braucht einen OpenAI API-Schluessel und muss diesen in den Einstellungen hinterlegen.**

### Funktionen

#### Beitraege

Beitraege koennen direkt an OpenAI gesendet werden und eine Antwort wird direkt als "Chat" unterhalb des Beitrages angezeigt.

#### Exportieren/Importieren der Einstellungen

Im Einstellungsmenue gibt es die Moeglichkeit alle Einstellungen in einem JSON Format zu exportieren und wieder zu importieren. Dieser Import ist inkrementel, also man kann auch die Daten von anderen Leuten importieren ohne die eignen zu verlieren, aber es gibt aber auch die Moeglichkeit alle Einstellungen zu loeschen.


Tags
----

Tags ist ein volles Schlagwort-System fuer das gesamte Forum und vereint die Funktionalitaet von User-Tags und User-Soft-Block und erweitert diese noch. Man kann Tags auf Benutzern, Themen und Foren selbst erfassen und diese verwalten. Zusaetzlich kann man auf den einzelnen Tags Effekte erfassen wie man dies bereits von User-Soft-Block gewoehnt ist.

**Ein gemeinsamer Betrieb mit User-Tags und User-Soft-Block ist nicht implementiert, da sich die Skripte teilweise sehr stark ueberschneiden.**

### Funktionen

#### Tags

Tags koennen auf Benutzern, Themen und Foren erfasst werden.

Tags koennen eine Farbe, Text und eine Notiz haben. Der Text wird direkt als HTML gesetzt, man kann also HTML Elemente in diesem verwenden. Ebenfalls gibt es die Moeglichkeit die Schriftart auf Font Awesome umzuschalten, damit ist es moeglich im Text des Tags Font Awesome Symbole zu verwenden, diese muessen als HTML Entitaet geschrieben werden. Die Notiz wird als Tooltip auf dem Tag angezeigt.

Zusaetzlich koennen Effekte auf den Tags eingestellt werden welche auf das jeweilige Element wirken:

 * Farbige Hervorhebung (Weiss (#ffffff) wird als "keine Farbe" verwendet).
 * Durchsichtig schalten.
 * Unscharf schalten.
 * Optionales aufheben der Filter wenn man mit der Maus ueber das Element faehrt.

Da man mehrere Tags mit aehnlichen Effekten haben kann, wird jeweils der letzte Tag mit desem Effekte angewendet. Die Reihenfolge der Tags kann man dahingehend aendern.

#### Menue

Oben in der Benutzerleiste wird ein zusaetzliches Icon eingeblendet ueber welches man die Einstellungen fuer das UserScript aendern und anpassen kann.

#### Vorlagen

Man kann in den Einstellungen Vorlagen erfassen, welche man dann nur noch anklicken muss um diese hinzuzufuegen.

#### Verwaltung

In den Einstellungen gibt es die Moeglichkeit alle Elemente mit Tags aufzulisten und diese direkt in dieser Liste zu bearbeiten.

#### Importieren von User-Tags und User-Soft-Block

Es gibt die Moeglichkeit die exportierten Daten von User-Soft-Block und User-Tags zu importieren und somit zu uebernehmen.

#### Exportieren/Importieren der Einstellungen

Im Einstellungsmenue gibt es die Moeglichkeit alle Einstellungen und erfassten Tags in einem JSON Format zu exportieren und wieder zu importieren. Dieser Import ist inkrementel, also man kann auch die Daten von anderen Leuten importieren ohne die eignen zu verlieren, aber es gibt aber auch die Moeglichkeit alle Einstellungen zu loeschen.


User-Soft-Block
---------------

User-Soft-Block erlaubt es einzelne Benutzer "weich" zu blockieren. Hierzu kann man Effekte auf jedem einzelnen Benutzer anlegen wonach die Beitraege von diesem Benutzer dann entweder leicht durchsichtig, unscharf oder auch beides gleichzeitig werden. Ebenfalls koennen die Beitraege optional durch bewegen der Maus ueber diese wieder lesbar gemacht werden.

### Funktionen

#### Effekte auf Benutzern

 * Farbige Hervorhebung des Benutzers (Weiss (#ffffff) wird als "keine Farbe" verwendet).
 * Durchsichtig schalten der Beitraege, des Avatars und des Benutzernamens.
 * Unscharf schalten  der Beitraege, des Avatars und des Benutzernamens.
 * Optionales aufheben der Filter wenn man mit der Maus ueber das Element faehrt.

#### Menue

Oben in der Benutzerleiste wird ein zusaetzliches Icon eingeblendet ueber welches man die Einstellungen fuer das UserScript aendern und anpassen kann.

#### Exportieren/Importieren der Einstellungen

Im Einstellungsmenue gibt es die Moeglichkeit alle Einstellungen und erfassten Soft-Blocks in einem JSON Format zu exportieren und wieder zu importieren. Dieser Import ist inkrementel, also man kann auch die Daten von anderen Leuten importieren ohne die eignen zu verlieren, aber es gibt aber auch die Moeglichkeit alle Einstellungen zu loeschen.


User-Tags
---------

User-Tags erlaubt es Schlagwoerter/Heftnotizen auf einzelnen Benutzern zu erfassen. Also man kann einzelne Benutzer mit Tags versehen um zusaetzliche Informaionen ueber diesen Benutzer zu halten.

### Funktionen

#### Tags

Tags auf einem Benutzer koennen eine Farbe, Text und eine Notiz haben. Der Text wird direkt als HTML gesetzt, man kann also HTML Elemente in diesem verwenden. Ebenfalls gibt es die Moeglichkeit die Schriftart auf Font Awesome umzuschalten, damit ist es moeglich im Text des Tags Font Awesome Symbole zu verwenden, diese muessen als HTML Entitaet geschrieben werden. Die Notiz wird als Tooltip auf dem Tag angezeigt.

#### Menue

Oben in der Benutzerleiste wird ein zusaetzliches Icon eingeblendet ueber welches man die Einstellungen fuer das UserScript aendern und anpassen kann.

#### Vorlagen

Man kann in den Einstellungen Vorlagen erfassen, welche man dann nur noch anklicken muss um diese hinzuzufuegen.

#### Exportieren/Importieren der Einstellungen

Im Einstellungsmenue gibt es die Moeglichkeit alle Einstellungen und erfassten Tags in einem JSON Format zu exportieren und wieder zu importieren. Dieser Import ist inkrementel, also man kann auch die Daten von anderen Leuten importieren ohne die eignen zu verlieren, aber es gibt aber auch die Moeglichkeit alle Einstellungen zu loeschen.
